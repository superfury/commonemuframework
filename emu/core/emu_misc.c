/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/emu/gpu/gpu_text.h" //Emulator support!
#include "headers/emu/directorylist.h" //Directory listing support!
#include "headers/emu/threads.h" //For terminating all threads on a breakpoint!
#include "headers/emu/gpu/gpu_framerate.h" //For refreshing the framerate surface only!
#include "headers/fopen64.h" //64-bit fopen support!
#include "headers/support/locks.h" //Locking support!
#include "headers/emu/emu_misc.h" //Our own definitions!

uint_32 convertrel(uint_32 src, uint_32 fromres, uint_32 tores) //Relative int conversion!
{
	DOUBLE data;
	data = (DOUBLE)src; //Load src!
	if (fromres!=0)
	{
		data /= (DOUBLE)fromres; //Divide!
	}
	else
	{
		data = 0.0f; //Clear!
	}
	data *= (DOUBLE)tores; //Generate the result!
	return (int)data; //Give the result!
}

int safe_vsnprintf(char* s, size_t size, const char* format, va_list arg)
{
	int result;
	result = vsnprintf(s, size, format, arg); //Normal call
	s[size - 1] = (char) 0; //Safety
	return result;
}

//CONCAT support!
char concatinations_constsprintf[256];
char *constsprintf(char *text, ...)
{
	cleardata(&concatinations_constsprintf[0],sizeof(concatinations_constsprintf)); //Init!
	va_list args; //Going to contain the list!
	va_start (args, text); //Start list!
	safevsnprintf (concatinations_constsprintf,sizeof(concatinations_constsprintf), text, args); //Compile list!
	va_end (args); //Destroy list!
	
	return &concatinations_constsprintf[0]; //Give the concatinated string!
}

extern GPU_TEXTSURFACE *frameratesurface; //The framerate surface!
void BREAKPOINT() //Break point!
{
	termThreads(); //Terminate all other threads!
	GPU_text_locksurface(frameratesurface);
	GPU_textgotoxy(frameratesurface,0,0); //Left-up!
	GPU_textprintf(frameratesurface,RGB(0xFF,0xFF,0xFF),RGB(0,0,0),"Breakpoint reached!");
	GPU_text_releasesurface(frameratesurface);
	renderFramerateOnly(); //Render the framerate surface only!
	unlock(LOCK_MAINTHREAD); //Make us quittable!
	dosleep(); //Stop!
}

/*

FILE_EXISTS

*/

int FILE_EXISTS(char *filename)
{
	BIGFILE *f = emufopen64(filename,"r"); //Try to open!
	if (!f) //Failed?
	{
		if (emufileopened(filename)) //Already opened?
		{
			return 2; //Exists, but opened already!
		}
		return 0; //Doesn't exist!
	}
	emufclose64(f); //Close the file!
	return 1; //Exists!
}

char *substr(char *s,size_t size,int startpos) //Simple substr function, with string and starting position!
{
	if ((int)safestrlen(s,size)<(startpos+1)) //Out of range?
	{
		return NULL; //Nothing!
	}
	return (char *)s+startpos; //Give the string from the start position!
}

void delete_file(char *directory, char *filename)
{
	char filename2[256];
	if (!filename) return; // Not a file?
	if (*filename=='*') //Wildcarding?
	{
		if (!directory) return; //Not a directory?
		memset(&filename2, 0, sizeof(filename2)); //Init!
		safe_strcpy(filename2, sizeof(filename2), filename); //Local copy!
		char *f2 = substr(filename2,sizeof(filename2),1); //Take off the *!

		char direntry[256];
		byte isfile;
		DirListContainer_t dir;
		if (!opendirlist(&dir,directory,&direntry[0],&isfile,0))
		{
			return; //Nothing found!
		}
		
		/* open directory */
		do //Files left to check?
		{
			if (direntry[0] == '.') continue; //. or ..?
			if (strcmp(substr(direntry,sizeof(direntry),(int)safestrlen(direntry,sizeof(direntry))-(int)safestrlen(f2,255)),f2)==0) //Match? Note: sizeof(filename2(=f2) is one less because of substr)
			{
				delete_file(directory,direntry); //Delete the file!
			}
		}
		while (readdirlist(&dir,&direntry[0],&isfile)); //Files left to check?)
		closedirlist(&dir); //Close the directory!
		return; //Don't process normally!
	}
	//Compose the full path!
	char fullpath[256];
	memset(&fullpath,0,sizeof(fullpath)); //Clear the directory!
	if (directory) //Directory specified?
	{
		safestrcpy(fullpath,sizeof(fullpath),directory);
		safestrcat(fullpath,sizeof(fullpath),PATHSEPERATOR);
	}
	else
	{
		safestrcpy(fullpath,sizeof(fullpath),""); //Start without directory, used by the filename itself!
	}
	safestrcat(fullpath,sizeof(fullpath),filename); //Full filename!

	BIGFILE *f = emufopen64(fullpath,"r"); //Try to open!
	if (f) //Found?
	{
		emufclose64(f); //Close it!
		removefile(fullpath); //Remove it, ignore the result!
	}
}


/*

Safe strlen function.

*/

#if (defined(IS_LINUX) || defined(IS_PS3)) && !defined(IS_ANDROID)
size_t linux_safe_strnlen(const char *s, size_t count)
{
	const char *sc;

	for (sc = s; count-- && *sc != '\0'; ++sc)
		/* nothing */;
	return sc - s;
}
#endif

uint_32 safe_strlen(const char *str, size_t size)
{
	if (str) //Valid?
	{
		//Valid address determined!
		if (!size) //Unlimited?
		{
			return (uint_32)strlen(str); //Original function with unlimited length!
		}
		#if (defined(IS_LINUX) || defined(IS_PS3)) && !defined(IS_ANDROID)
		return linux_safe_strnlen(str,(size_t)size); //Limited length, faster option!
		#else
		return (uint_32)strnlen(str,(size_t)size); //Limited length, faster option!
		#endif
	}
	return 0; //No string = no length!
}

void safe_strcpy(char *dest, size_t size, const char *src)
{
	strncpy(dest,src,(MAX(size,1)-1));
	dest[MAX(size,1)-1]=0; //Proper termination always!
}

void safe_strcat(char *dest, size_t size, const char *src)
{
	size_t length;
	if (unlikely(size == 0)) return; //Invalid size!
	if (unlikely(dest[size - 1])) //Not properly terminated?
	{
		dest[size - 1] = 0; //Terminate properly first!
	}
	if (unlikely(size == 1)) return; //Destination is too small to add anything to!
	length = safe_strlen(dest, size); //How much is used?
	if (unlikely(length >= (size-1))) //Limit already reached?
	{
		return; //Don't do anything when unavailable!
	}
	length = (size-1)-length; //How much size is available, including a NULL character in the destination?
	if (unlikely(!length)) return; //Abort if nothing can be added!
	strncat(dest, src, length); //Append source to the destination, with up to the available length only!
	dest[MAX(size, 1) - 1] = 0; //Proper termination always!
}

char safe_scatnprintfbuf[65536];  // A small buffer for the result to be contained!
void safe_scatnprintf(char *dest, size_t size, const char *src, ...)
{
	safe_scatnprintfbuf[0] = 0; //Simple init!
	safe_scatnprintfbuf[65535] = 0; //Simple init!
	va_list args; //Going to contain the list!
	va_start (args, src); //Start list!
	safevsnprintf (safe_scatnprintfbuf,sizeof(safe_scatnprintfbuf), src, args); //Compile list!
	va_end (args); //Destroy list!
	safe_scatnprintfbuf[65535] = 0; //Simple finish safety!
	//Now, the buffer is loaded, append it safely!
	safe_strcat(dest, size, safe_scatnprintfbuf); //Add the buffer to concatenate to the result, safely!
}

//Same as FILE_EXISTS?
int file_exists(char *filename)
{
	return FILE_EXISTS(filename); //Alias!
}

int move_file(char *fromfile, char *tofile)
{
	int result;
	if (file_exists(fromfile)) //Original file exists?
	{
		if (file_exists(tofile)) //Destination file exists?
		{
			if ((result = removefile(tofile))!=0) //Error removing destination?
			{
				return result; //Error code!
			}
		}
		return renamefile(fromfile,tofile); //0 on success, anything else is an error code..
	}
	return 0; //Error: file doesn't exist, so success!
}

float RandomFloat(float min, float max)
{
    if (min>max) //Needs to be swapped?
    {
	float temp;
	temp = min;
	min = max;
	max = temp;
    }

    float random;
    random = ((float) rand()) / (float) RAND_MAX;

    DOUBLE range = max - min;  
    return (float)((DOUBLE)((random*range) + (DOUBLE)min)); //Give the result within range!
}

float frand() //Floating point random
{
	return RandomFloat(FLT_MIN,FLT_MAX); //Generate a random float!
}

short RandomShort(short min, short max)
{
	float temp;
	retryOOR:
	temp = floorf(RandomFloat((float)min, (float)(((int_32)max)+1))); //Range! Take 0.9 extra on the range for equal range chance!
	if (unlikely(((short)temp)>max)) goto retryOOR; //Retry when out of range (border case)!
	return (short)LIMITRANGE(temp,min,max); //Short random generator, equal spread!
}

short shortrand() //Short random
{
	return (short)RandomShort(-SHRT_MAX,SHRT_MAX); //Short random generator!
}

uint_32 converthex2int(char* s)
{
	uint_32 result = 0; //The result!
	char* temp;
	byte tempnr;
	for (temp = s; *temp; ++temp)
	{
		switch (*temp) //What character?
		{
		case '0': tempnr = 0x0; break;
		case '1': tempnr = 0x1; break;
		case '2': tempnr = 0x2; break;
		case '3': tempnr = 0x3; break;
		case '4': tempnr = 0x4; break;
		case '5': tempnr = 0x5; break;
		case '6': tempnr = 0x6; break;
		case '7': tempnr = 0x7; break;
		case '8': tempnr = 0x8; break;
		case '9': tempnr = 0x9; break;
		case 'A': tempnr = 0xA; break;
		case 'B': tempnr = 0xB; break;
		case 'C': tempnr = 0xC; break;
		case 'D': tempnr = 0xD; break;
		case 'E': tempnr = 0xE; break;
		case 'F': tempnr = 0xF; break;
		default: //Unknown character?
			return 0; //Abort without result! Can't decode!
		}
		result <<= 4; //Shift our number high!
		result |= tempnr; //Add in our number to the resulting numbers!
	}
	return result; //Give the result we calculated!
}

//A is always the most significant part, k is the shift to apply

void shiftl256(uint_64 *a, uint_64 *b, uint_64 *c, uint_64 *d, size_t k)
{
	if (unlikely(k >= 256)) //256 bits to shift?
	{
		*a = *b = *c = *d = 0; //Clear all!
		return; //Abort!
	}
	if (k >= 64) // shifting a 64-bit integer by more than 63 bits is "undefined"
	{
		retryshiftl256:
		*a = *b;
		*b = *c;
		*c = *d;
		*d = 0;
		k -= 64;
		if (unlikely(k >= 64)) goto retryshiftl256; //Retry if needed!
	}
	if (likely(k)) //Anything to shift at all? 0-bit shifts don't work here!
	{
		//Shifting less than 64 bits!
		*a = (*a << k) | (*b >> (64 - k));
		*b = (*b << k) | (*c >> (64 - k));
		*c = (*c << k) | (*d >> (64 - k));
		*d = (*d << k);
	}
}

void shiftr256(uint_64 *a, uint_64 *b, uint_64 *c, uint_64 *d, size_t k)
{
	if (unlikely(k >= 256))
	{
		*a = *b = *c = *d = 0; //Clear all!
		return; //Abort!
	}
	if (k >= 64) // shifting a 64-bit integer by more than 63 bits is "undefined"
	{
		retryshiftr256:
		*d = *c;
		*c = *b;
		*b = *a;
		*a = 0;
		if (unlikely(k >= 64)) goto retryshiftr256; //Retry if needed!
	}
	if (likely(k)) //Anything to shift at all? 0-bit shifts don't work here!
	{
		//Shifting less than 64 bits!
		*d = (*c << (64 - k)) | (*d >> k);
		*c = (*b << (64 - k)) | (*c >> k);
		*b = (*a << (64 - k)) | (*b >> k);
		*a = (*a >> k);
	}
}

//128 bits shift based on the above.
void shiftl128(uint_64* a, uint_64* b, size_t k)
{
	if (unlikely(k >= 128))
	{
		*a = *b = 0; //Clear all!
		return; //Abort!
	}
	if (k >= 64) // shifting a 64-bit integer by more than 63 bits is "undefined"
	{
		*a = *b;
		*b = 0;
		k -= 64; //Remainde to shift!
	}
	if (likely(k)) //Anything to shift at all? 0-bit shifts don't work here!
	{
		//Shifting less than 64 bits!
		*a = (*a << k) | (*b >> (64 - k));
		*b = (*b << k);
	}
}

void shiftr128(uint_64* a, uint_64* b, size_t k)
{
	if (unlikely(k >= 128))
	{
		*a = *b = 0; //Clear all!
		return; //Abort!
	}
	if (k >= 64) // shifting a 64-bit integer by more than 63 bits is "undefined"
	{
		*b = *a;
		*a = 0;
		k -= 64; //Remainder to shift!
	}
	//Shifting less than 64 bits!
	if (likely(k)) //Anything to shift at all? 0-bit shifts don't work here!
	{
		*b = (*a << (64 - k)) | (*b >> k);
		*a = (*a >> k);
	}
}

#define SDL_RUNTIME_VERSION_ATLEAST(C1, C2, C3, X, Y, Z) \
    (SDL_VERSIONNUM(C1,C2,C3) >= SDL_VERSIONNUM(X, Y, Z))

byte isSDL_runtime_version_atleast(uint_32 x, uint_32 y, uint_32 z) //Runtime version detection to be at least some version!
{
#if defined(SDL2) || defined(SDL3)
#ifdef SDL3
	#ifndef SDL_VERSIONNUM_MICRO
	//Old SDL3 method?
	SDL_Version linked;
	SDL_GetVersion(&linked); //Get the version we're linked against!
	return SDL_RUNTIME_VERSION_ATLEAST((uint_32) linked.major, (uint_32) linked.minor, (uint_32) linked.patch, x, y, z); //Are we at least linked against this version?
	#else
	int linked;
	linked = SDL_GetVersion(); //Get the version we're linked against!
	return SDL_RUNTIME_VERSION_ATLEAST(SDL_VERSIONNUM_MAJOR(linked), SDL_VERSIONNUM_MINOR(linked), SDL_VERSIONNUM_MICRO(linked), x, y, z);//Are we at least linked against this version?		#endif
	#endif
	#else
	//SDL2?
	SDL_version linked;
	SDL_GetVersion(&linked);  //Get the version we're linked against!
	return SDL_RUNTIME_VERSION_ATLEAST((uint_32) linked.major, (uint_32) linked.minor, (uint_32) linked.patch, x, y, z); //Are we at least linked against this version?
	#endif
#else
	//SDL1 doesn't allow multiple versions, so perform static link check?
	return SDL_VERSION_ATLEAST(x, y, z); //Give the generic macro, if any is supported!
#endif
}

void convert_ASCII_to_UTF8(char *dest, size_t size, char c)
{
	if ((size < 3) && (c & 0x80U)) //Not enough to fill (extended character)?
	{
		*dest = 0; //Safely terminate!
		return;
	}
	if (size < 2) //Not enough to fill always (1 character + terminating character)?
	{
		*dest = 0; //Safely terminate!
		return;
	}
	dest[0] = 0; //Init string to fill!
	if (!convert_codepoint_to_UTF8(dest, (((uint_32)c) & 0xFF), size)) //Failed to convert?
	{
		dest[0] = 0; //Keep empty string!
	}
}

char UTF8codeASCII[5] = ""; //Up to max length code point (4 bytes) with terminating character!
char* ASCII_to_UTF8(char c)
{
	convert_ASCII_to_UTF8(&UTF8codeASCII[0], sizeof(UTF8codeASCII), c); //Convert!
	return &UTF8codeASCII[0]; //Give the string buffer!
}

byte convert_codepoint_to_UTF8(char *dst, uint_32 codepoint, size_t usize)
{
	uint_32 c;
	uint_32 usizeleft;
	byte outputbuffer[4]; //encoded result buffer
	byte outputbuffersize; //length of the output buffer
	char *dstpos, *lastdstpos;
	if (!dst) return 0; //No destination?
	if (!usize) return 0; //Abort on invalid size!
	dstpos = lastdstpos = dst; //Where to start writing at.
	usizeleft = usize; //How many bytes are left on the destination?
	//Read and decode the UTF-8 string into the destination!
	c = codepoint; //Character to render, as a codepoint!
	//UTF-8 is 6 bits per byte, except the first byte or single byte ASCII encoding.
	if (c > 0x1FFFF) //Out-of-range?
	{
		*dst = 0; //Invalid!
		return 0; //Not convertible to UTF-8!
	}
	else if (c >= 0x10000) //4 bytes?
	{
		outputbuffer[0] = 0xF0 | ((c >> 18) & 0x7); //First byte!
		outputbuffer[1] = 0x80 | ((c >> 12) & 0x3F); //Second byte!
		outputbuffer[2] = 0x80 | ((c >> 6) & 0x3F); //Second byte!
		outputbuffer[3] = 0x80 | (c & 0x3F); //Third byte!
		outputbuffersize = 4; //4 bytes long
	}
	else if (c >= 0x800) //3 bytes?
	{
		outputbuffer[0] = 0xE0 | ((c >> 12) & 0xF); //First byte!
		outputbuffer[1] = 0x80 | ((c >> 6) & 0x3F); //Second byte!
		outputbuffer[2] = 0x80 | (c & 0x3F); //Third byte!
		outputbuffersize = 3; //3 bytes long
	}
	else if (c >= 0x80) //2 bytes?
	{
		outputbuffer[0] = 0xC0 | ((c >> 6) & 0x1F); //First byte!
		outputbuffer[1] = 0x80 | (c & 0x3F); //Second byte!
		outputbuffersize = 2; //2 bytes long
	}
	else //1 byte?
	{
		outputbuffer[0] = (byte)c; //Single byte!
		outputbuffersize = 1; //1 byte long!
	}
	if (usizeleft > outputbuffersize) //Enough size left (and leaving the final character)?
	{
		lastdstpos = dstpos; //Update last written location to the start of the last converted character!
		for (c = 0; c < outputbuffersize; ++c) //Add the bytes to the result!
		{
			*(dstpos++) = outputbuffer[c]; //Write a single byte to the output!
		}
		usizeleft -= outputbuffersize; //Parse what's converted and what is left!
	}
	else //Not enough size left?
	{
		*dst = 0; //Terminate to clear safely!
		return 0; //Stop converting: ran out of UTF-8 space!
	}
	if (usizeleft) //proper termination?
	{
		*dstpos = 0; //Terminator!
	}
	else //Invalid termination?
	{
		*lastdstpos = 0; //Terminator!
	}

	//Now the string is entirely converted from codepoint to UTF-8!
	return outputbuffersize;
}

#ifdef IS_WINDOWS
//Windows requires converting to/from unicode and UTF-8!
#if defined(UNICODE) || defined(_UNICODE)
void convert_UTF8_to_wchar(TCHAR* w, size_t wsize, char* src)
{
	byte is_UTF8codepoint; //Use UTF-8 code point?
	uint_32 UTF8codepoint = 0; //UTF-8 code point!
	byte UTF8bytesleft; //How many bytes are left for UTF-8 parsing?
	uint_32 c;
	size_t wsizeleft;
	uint_32 i;
	if ((!w) || (!src)) return; //Invalid data?
	wsizeleft = wsize / sizeof(*w); //How many items are left?
	//Read and decode the UTF-8 string into the destination!
	UTF8bytesleft = 0; //Init!
	for (i = 0; i < (int)strlen(src); i++) //Process UTF-8 text!
	{
		is_UTF8codepoint = 0; //Not UTF-8!
		if (src[i] & 0x80) //UTF-8 support!
		{
			if ((src[i] & 0xC0) == 0x80) //Continuation byte?
			{
				if (UTF8bytesleft) //Were we paring UTF-8?
				{
					--UTF8bytesleft; //One byte parsed!
					//6 bits added!
					UTF8codepoint <<= 6; //6 bits to add!
					UTF8codepoint |= (src[i] & 0x3F); //6 bits added!
					if (UTF8bytesleft) //Still more bytes left?
					{
						continue; //Next byte please!
					}
					else //Finished UTF-8 code point?
					{
						is_UTF8codepoint = 1; //UTF-8!
					}
				}
				else //Invalid UTF-8 string, abort it! Count as extended ASCII!
				{
					UTF8bytesleft = 0; //Abort!
				}
			}
			else if ((src[i] & 0xE0) == 0xC0) //Two byte UTF-8 starting?
			{
				//5 bits for the first byte, 6 for the other bytes!
				UTF8codepoint = (src[i] & 0x1F); //5 bits to start with!
				UTF8bytesleft = 1; //1 byte left!
				continue; //Next byte please!
			}
			else if ((src[i] & 0xF0) == 0xE0) //Three byte UTF-8 starting?
			{
				//4 bits for the first byte, 6 for the other bytes!
				UTF8codepoint = (src[i] & 0xF); //4 bits to start with!
				UTF8bytesleft = 2; //2 bytes left!
				continue; //Next byte please!
			}
			else if ((src[i] & 0xF8) == 0xF0) //Four byte UTF-8 starting?
			{
				//3 bits for the first byte, 6 for the other bytes!
				UTF8codepoint = (src[i] & 0x7); //3 bits to start with!
				UTF8bytesleft = 3; //3 bytes left!
				continue; //Next byte please!
			}
			else //Non-UTF-8 encoded?
			{
				//Finish UTF-8 parsing!
				UTF8bytesleft = 0; //End UTF-8 parsing!
				//Handle the character normally as ASCII!
			}
		}
		c = src[i]; //Character to convert!
		if (is_UTF8codepoint) //UTF-8?
		{
			c = UTF8codepoint; //Set the code point to use!
		}
		if (wsizeleft > 1) //More than 1 entry left?
		{
			if (c <= (1 << (sizeof(*w) << 3))) //Within range of the codepoint of the destination?
			{
				*(w++) = c;
				--wsizeleft; //One item less left!
			}
		}
	}
	if (wsizeleft) //proper termination?
	{
		*w = 0; //Terminator!
	}
	else
	{
		--w; //Last character!
		*w = 0; //Terminator!
	}

	//Now the string is entirely converted from UTF-8 to WCHAR!
}
void convert_wchar_to_UTF8(char* dst, TCHAR* w, size_t usize)
{
	uint_32 c;
	size_t usizeleft;
	byte outputbuffer[4]; //encoded result buffer
	byte outputbuffersize; //length of the output buffer
	TCHAR* d;
	char* dstpos, * lastdstpos;
	if (!usize) return; //Abort on invalid size!
	if ((!dst) || (!w)) return; //Invalid data?
	d = w; //Init position!
	dstpos = lastdstpos = dst; //Where to start writing at.
	usizeleft = usize; //How many bytes are left on the destination?
	//Read and decode the UTF-8 string into the destination!
	for (; *d;) //Process WCHAR text!
	{
		c = ((uint_32) * (d++)); //Character to render, as a codepoint!
		//UTF-8 is 6 bits per byte, except the first byte or single byte ASCII encoding.
		if (c > 0x1FFFF) //Out-of-range?
		{
			continue; //Skip characters that are out-of-range!
		}
		else if (c >= 0x10000) //4 bytes?
		{
			outputbuffer[0] = 0xF0 | ((c >> 18) & 0x7); //First byte!
			outputbuffer[1] = 0x80 | ((c >> 12) & 0x3F); //Second byte!
			outputbuffer[2] = 0x80 | ((c >> 6) & 0x3F); //Second byte!
			outputbuffer[3] = 0x80 | (c & 0x3F); //Third byte!
			outputbuffersize = 4; //4 bytes long
		}
		else if (c >= 0x800) //3 bytes?
		{
			outputbuffer[0] = 0xE0 | ((c >> 12) & 0xF); //First byte!
			outputbuffer[1] = 0x80 | ((c >> 6) & 0x3F); //Second byte!
			outputbuffer[2] = 0x80 | (c & 0x3F); //Third byte!
			outputbuffersize = 3; //3 bytes long
		}
		else if (c >= 0x80) //2 bytes?
		{
			outputbuffer[0] = 0xC0 | ((c >> 6) & 0x1F); //First byte!
			outputbuffer[1] = 0x80 | (c & 0x3F); //Second byte!
			outputbuffersize = 2; //2 bytes long
		}
		else //1 byte?
		{
			outputbuffer[0] = (byte)c; //Single byte!
			outputbuffersize = 1; //1 byte long!
		}
		if (usizeleft > outputbuffersize) //Enough size left (and leaving the final character)?
		{
			lastdstpos = dstpos; //Update last written location to the start of the last converted character!
			for (c = 0; c < outputbuffersize; ++c) //Add the bytes to the result!
			{
				*(dstpos++) = outputbuffer[c]; //Write a single byte to the output!
			}
			usizeleft -= outputbuffersize; //Parse what's converted and what is left!
		}
		else //Not enough size left?
		{
			break; //Stop converting: ran out of UTF-8 space!
		}
	}
	if (usizeleft) //proper termination?
	{
		*dstpos = 0; //Terminator!
	}
	else //Invalid termination?
	{
		*lastdstpos = 0; //Terminator!
	}

	//Now the string is entirely converted from WCHAR to UTF-8!
}
#endif
#endif

uint_32 CP437_translatetbl0[0x20] = {
	0x0000, 0x263A, 0x263B, 0x2665, 0x2666, 0x2663, 0x2660, 0x2022, 0x25D8, 0x25CB, 0x25D9, 0x2642, 0x2640, 0x266A, 0x266B, 0x263C,
	0x25BA, 0x25C4, 0x2195, 0x203C, 0x00B6, 0x00A7, 0x25AC, 0x21A8, 0x2191, 0x2193, 0x2192, 0x2190, 0x221F, 0x2194, 0x25B2, 0x25BC}; //Range 00-1F CP-437
uint_32 CP437_translatetbl1[0x80] = {
	0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7, 0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5, //80-8F
	0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9, 0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192, //90-9F
	0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA, 0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB, //A0-AF
	0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556, 0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510, //B0-BF
	0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F, 0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567, //C0-CF
	0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B, 0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580, //D0-DF
	0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4, 0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229, //E0-EF
	0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248, 0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0}; //F0-FF
	
uint_32 CP437_to_unicode(uint_32 cp437codepoint)
{
	if (cp437codepoint==0x7F) //DEL character?
	{
		return 0x2302; //House character
	}
	if ((cp437codepoint>=0x20) && (cp437codepoint<0x80)) //Direct mapped?
	{
		return cp437codepoint; //Direct mapped!
	}
	if (cp437codepoint < 0x20) //First map?
	{
		return CP437_translatetbl0[cp437codepoint&0x1F]; //First trnnslation map!
	}
	if (cp437codepoint < 0x100) //Second map?
	{
		return CP437_translatetbl1[cp437codepoint&0x7F]; //Second translation map!
	}
	return cp437codepoint; //Don't translate!
}