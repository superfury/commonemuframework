/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!

#include "headers/emu/emu_main.h" //Ourselves!

#include "headers/emu/emucore.h"
#include "headers/emu/gpu/gpu_text.h" //Text surface support!

//Disable BIOS&OS loading for testing?
#define NOEMU 0
#define NOBIOS 0

/*

BIOS Loader&Execution!

*/

byte use_profiler = 0; //To use the profiler?

byte emu_use_profiler()
{
	return use_profiler; //To use or not?
}

//Main loader stuff:

byte reset = 0; //To fully reset emu?
uint_32 romsize = 0; //For checking if we're running a ROM!

GPU_TEXTSURFACE* debugSurface = NULL;
Handler debugSurfaceHandler = NULL; //Text surface handler!

void registerDebugSurfaceHandler(Handler handler)
{
	debugSurfaceHandler = handler; //Register it!
}

void updateDebugSurface()
{
	if (debugSurfaceHandler) //Registered?
	{
		debugSurfaceHandler(); //Execute!
	}
}

void doneDebugSurface()
{
	GPU_removeTextSurface(debugSurface); //Unregister!
	free_GPUtext(&debugSurface); //Try to deallocate the BIOS Menu surface!
}

void initDebugSurface()
{
	if (debugSurface) //Already allocated?
	{
		doneDebugSurface(); //Finish first!
	}
	debugSurface = alloc_GPUtext(); //Allocate GPU text surface for us to use!
	if (!debugSurface) return; //Couldn't allocate the surface!
	GPU_addTextSurface(debugSurface, &updateDebugSurface); //Register our renderer!
}

extern byte EMU_RUNNING; //Emulator running? 0=Not running, 1=Running, Active CPU, 2=Running, Inactive CPU (BIOS etc.)

void finishEMU() //Called on emulator quit.
{
	doneEMU(); //Finish up the emulator that was still running!
}