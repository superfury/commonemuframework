/*

Copyright (C) 2021 - 2022 Superfury

This file is part of The Common Emulator Framework.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __EMU_SWITCH_H
#define __EMU_SWITCH_H

#include "headers/types_base.h" //Base types!
#include <sys/stat.h> //For mkdir
#include <switch.h> //Main Switch header

#define realdelay(x) (((x)>=1000)?((x)/1000):1)
#define delay(us) SDL_Delay(realdelay(us))
#define dosleep() for (;;) delay(1000000)

#define domkdir(path) mkdir(path, 0755)
#define removedirectory(dir) remove(dir)
#define removefile(file) remove(file)
#define renamefile(oldname,newname) rename(oldname,newname)

//INLINE options!
#ifdef OPTINLINE
#undef OPTINLINE
#endif

#ifdef __ENABLE_INLINE
#define OPTINLINE static inline
#else
#define OPTINLINE static
#endif

//Enum safety
#define ENUM8 :byte
#define ENUMS8 :sbyte
#define ENUM16 :word
#define ENUMS16 :sword
#define ENUM32 :uint_32
#define ENUMS32 :int_32

#ifdef _LP64
#undef LONG64SPRINTF
//Linux needs this too!
#define IS_64BIT
#define LONG64SPRINTF long long
typedef uint_64 ptrnum;
#else
typedef uint_32 ptrnum;
#endif

void sleepthread(uint_64 ns);

//We're Switch!
#define IS_SWITCH

//Not Visual C?
#ifdef SDL3
//Basic SDL for rest platforms!
#include <SDL3/SDL.h> //SDL library!
#else
#ifdef SDL2
//Basic SDL for rest platforms!
#include <SDL2/SDL.h> //SDL library!
#else
#ifdef IS_PSP
//We're handling main ourselves!
#define SDL_MAIN_HANDLED
#endif
#include <SDL/SDL.h> //SDL library!
#endif
#endif

#endif