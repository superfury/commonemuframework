/*

Copyright (C) 2025 - 2025 Superfury

This file is part of The Common Emulator Framework.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __EMU_PS3_H
#define __EMU_PS3_H

#include "headers/types_base.h" //Base types!
/*
* TODO: Implement the following on PS3?
#include <psp2/kernel/threadmgr.h>  //PSP kernel timing support!
*/
#include <lv2/process.h> //For exiting with sysProcessExit

#include <sys/file.h> //File and directory support
#include <sys/thread.h> //Yielding support!

#define realdelay(x) (((x)>=1000)?((x)/1000):1)
#define delay(us) if (us) { usleep(us); } sysThreadYield()
#define dosleep() for (;;) delay(1000000)

#define domkdir(dir) sysLv2FsMkdir(dir, 0755)
#define removedirectory(dir) sysLv2FsRmdir(dir)
//TODO: check remove file support
#define removefile(file) sysLv2FsUnlink(file)
#define renamefile(oldname,newname) sysLv2FsRename(oldname,newname)

//INLINE options!
#ifdef OPTINLINE
#undef OPTINLINE
#endif

#ifdef __ENABLE_INLINE
#define OPTINLINE static inline
#else
#define OPTINLINE static
#endif

//Enum safety
#define ENUM8 :byte
#define ENUMS8 :sbyte
#define ENUM16 :word
#define ENUMS16 :sword
#define ENUM32 :uint_32
#define ENUMS32 :int_32

#ifndef _LP64
//PS4 is 64-bit
#define _LP64
#endif

#ifdef _LP64
#define IS_64BIT
#undef LONG64SPRINTF
//Linux needs this too!
#define LONG64SPRINTF long long
typedef uint_64 ptrnum;
#else
typedef uint_32 ptrnum;
#endif

//We're PS3!
#define IS_PS3

#ifdef SDL3
//Basic SDL for rest platforms!
#include <SDL3/SDL.h> //SDL library!
#else
#ifdef SDL2
//Basic SDL for rest platforms!
#include <SDL2/SDL.h> //SDL library!
#else
#ifdef IS_PSP
//We're handling main ourselves!
#define SDL_MAIN_HANDLED
#endif
#include <SDL/SDL.h> //SDL library!
#endif
#endif

#endif