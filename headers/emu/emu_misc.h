/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EMU_MISC_H
#define EMU_MISC_H

#include "headers/types.h" //Required support for Windows detection!

int FILE_EXISTS(char *filename);
void BREAKPOINT(); //Break point!
int move_file(char *fromfile, char *tofile); //Move a file, gives an error code or 0!
float frand(); //Floating point random
float RandomFloat(float min, float max); //Random float within range!
short shortrand(); //Short random
short RandomShort(short min, short max);
uint_32 converthex2int(char* s);

//shiftl/r128/256: A is always the most significant part, k is the shift to apply
//256 bits shift
void shiftl256 (uint_64 *a, uint_64 *b, uint_64 *c, uint_64 *d, size_t k);
void shiftr256 (uint_64 *a, uint_64 *b, uint_64 *c, uint_64 *d, size_t k);
//128 bits shift
void shiftl128(uint_64 *a, uint_64 *b, size_t k);
void shiftr128(uint_64 *a, uint_64 *b, size_t k);

//Storage auto-detection, when supported!
void BIOS_DetectStorage(); //Auto-Detect the current storage to use, on start only!
void BIOS_LoadIO(int showchecksumerrors); //Loads basic I/O drives from BIOS!
void convert_ASCII_to_UTF8(char *dest, size_t size, char c);
//ASCII_to_UTF8: Converts a single ASCII character to a (memory shared) UTF-8 string. Only usable for one ASCII character at a time.
char *ASCII_to_UTF8(char c);

uint_32 CP437_to_unicode(uint_32 cp437codepoint); //Translates a CP437 code point to Unicode code point, if possible (otherwise, direct-mapped).
byte convert_codepoint_to_UTF8(char *dst, uint_32 codepoint, size_t usize); //Convert a UTF-8 codepoint to a UTF-8 string.

#ifdef IS_WINDOWS
//Windows requires converting to/from unicode and UTF-8!
#if defined(UNICODE) || defined(_UNICODE)
void convert_UTF8_to_wchar(TCHAR* w, size_t wsize, char* src);
void convert_wchar_to_UTF8(char* dst, TCHAR* w, size_t usize);
#endif
#endif

#endif