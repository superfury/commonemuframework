/*

Copyright (C) 2022 - 2022 Superfury

This file is part of The Common Emulator Framework.

UniPCemu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UniPCemu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UniPCemu.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MD5_H
#define MD5_H

#include "headers/types.h"

typedef struct {
	uint64_t size;        // Size of input in bytes
	uint32_t buffer[4];   // Current accumulation of hash
	uint8_t input[64];    // Input to be used in the next step
	uint8_t digest[16];   // Result of algorithm
}MD5Context;

void md5Init(MD5Context* ctx);
void md5Update(MD5Context* ctx, uint8_t* input, size_t input_len);
void md5Finalize(MD5Context* ctx);
void md5Step(uint32_t* buffer, uint32_t* input);

uint32_t rotateLeft(uint32_t x, uint32_t n);

#endif