/*

Copyright (C) 2019 - 2023 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "headers/types.h" //Basic types!

//Internal handlers for the common emulator framework:
void onKeyPress(char *key); //On key press/hold!
byte onKeyRelease(char *key); //On key release!
void ReleaseKeys(); //Force release all normal keys (excluding ctrl,alt&shift) currently pressed!
void initEMUKeyboard(); //Initialise the keyboard support for emulating!

//Below function is implemented by the program itself.
byte EMU_keyboard_handler(byte key, byte pressed, byte ctrlispressed, byte altispressed, byte shiftispressed); //A key has been pressed (with interval) or released CALLED BY HARDWARE KEYBOARD (Virtual Keyboard?)? 0 indicates failure sending it!

//Get and set functions for the repeat rate and delay!
float HWkeyboard_getrepeatrate(); //Which repeat rate to use after the repeat delay! (chars/second)
void HWkeyboard_setrepeatrate(float rate);
float HWkeyboard_getrepeatdelay(); //Delay after which to start using the repeat rate till release! (in ms)
void HWkeyboard_setrepeatdelay(float delay);
byte HWkeyboard_getLEDs();
void HWkeyboard_setLEDs(byte LEDs);

#endif