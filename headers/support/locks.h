/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __LOCKS_H
#define __LOCKS_H
#include "headers/types.h" //Basic types!

void initLocks();
byte lock(byte id);
void unlock(byte id);

semaphore_type *getLock(byte id); //For termination of locks!

#define LOCK_ZALLOC 0
#define LOCK_GPU 1
#define LOCK_VIDEO 2
#define LOCK_CPU 3
#define LOCK_TIMERS 4
#define LOCK_INPUT 5
#define LOCK_SHUTDOWN 6
#define LOCK_FRAMERATE 7
#define LOCK_MAINTHREAD 8
#define LOCK_SOUND 9
#define LOCK_PERFMON 10
#define LOCK_ALLOWINPUT 11
#define LOCK_THREADS 12
#define LOCK_DISKINDICATOR 13
#define LOCK_PCAP 14
#define LOCK_PCAPFLAG 15
#define LOCK_THREADHOLDERS 16
#define LOCK_SOUNDRECORDING 17
#define LOCK_FILES 18
#define LOCK_WIFI 19
//Finally MIDI locks, when enabled!
//#define MIDI_LOCKSTART 20

#endif
