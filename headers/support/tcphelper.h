/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TCPHELPER_H
#define TCPHELPER_H

#include "headers/types.h" //Basic types!
#include "headers/support/fifobuffer.h" //FIFO buffer for transferred data!

#if defined(SDL_NET) || defined(SDL2_NET) || defined(SDL3_NET)
#if defined(SDL2) && (defined(SDL2_NET) || defined(SDL3_NET))
#define GOTNET
#else
#if defined(SDL3) && (defined(SDL3_NET))
#define GOTNET
#else
#ifdef SDL_NET
#define GOTNET
#endif
#endif
#endif
#endif

//Based on http://stephenmeier.net/2015/12/23/sdl-2-0-tutorial-04-networking/

typedef byte (*TCPCLIENTVALIDATIONFUNC)(uint_32 ipaddress, word port); //Validation functionality!
typedef byte (*TCPCLIENTVALIDATIONV6FUNC)(word *ipv6address, word port); //Validation functionality!

//General support for the backend.
void initTCP(); //Initialize us!
void doneTCP(void); //Finish us!

//Server side
byte TCP_ConnectServer(word port, word numConnections); //Try to connect as a server. 1 when successful, 0 otherwise(not ready).
byte TCPServerRunning(); //Is the server running?
byte TCPserverincoming(); //Is an connection incoming for acceptTCPServer?
sword acceptTCPServer(TCPCLIENTVALIDATIONFUNC validationfunc, TCPCLIENTVALIDATIONV6FUNC validationV6func); //Accept and connect when asked of on the TCP server! 1=Connected to a client, 0=Not connected. validationfunc can be called to validate a client to be accepted.
void TCPServer_Unavailable(); //We can't accept TCP connection now?
void stopTCPServer(); //Stop the TCP server!

//Client side(also used when the server is connected).
sword TCP_ConnectClient(const char *destination, word port); //Connect as a client! Call TCP_Connected afterwards if successfull to get the connection status until connected or failed.
byte TCP_Connected(sword id); //Is the client connected? 0=Disconnected, 1=Connected, 2=Resolving.
byte TCP_getremote(sword id, uint_32* ipaddr, word* port); //Retrieves the remote client information (IPv4)!
byte TCP_getremoteV6(sword id, word* ipaddr, word* port); //Retrieves the remote client information (IPv6)!
byte TCP_SendData(sword id, byte data); //Send data to the other side(both from server and client).
sbyte TCP_ReceiveData(sword id, byte *result); //Receive data, if available. 0=No data, 1=Received data, -1=Not connected anymore!
byte TCP_DisconnectClientServer(sword id); //Disconnect either the client or server, whatever state we're currently using.

#ifdef IS_PSP
#ifdef GOTNET
void allocTCPsurface(); //Allocate a TCP surface to use!
void freeTCPsurface(); //Free a TCP surface to use!
#endif
#endif

//WiFi support for devices supporting it's functionality to control it!
void connectWifi();
void disconnectWifi();
byte wifiConnected();
byte wifiGetIP(char* ip, size_t size);

/*
WifiSetAccessPointLocation: Sets the location for the access point inputs.
parameters:
	x: The x location
	y: The y location
result:
	0: Not changed (in use), 1: Changed, 2: Unsupported action.
*/

byte wifiSetAccessPointLocation(int x, int y);
#endif