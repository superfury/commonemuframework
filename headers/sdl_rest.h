/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMON_SDL_REST_H
#define COMMON_SDL_REST_H

//Basic 16/32/64-bit byte swapping support
#ifdef IS_PSP
#ifndef doSwapLE64
#define doSwapLE64(x) (x)
#endif
#ifndef doSwapLE32
#define doSwapLE32(x) (x)
#endif
#ifndef doSwapLE16
#define doSwapLE16(x) (x)
#endif
#ifndef doSwapBE16
//PSP doesn't support SDL_SwapBE16
#define doSwapBE16(x) ((((x)>>8)&0xFF)|(((x)&0xFF)<<8))
#endif
#ifndef doSwapBE32
//PSP doesn't support SDL_SwapBE32
#define doSwapBE32(x) (doSwapBE16(((x)>>16)&0xFFFF)|(doSwapBE16((x)&0xFFFF)<<16))
#endif
#endif

#if !SDL_VERSION_ATLEAST(3,1,2)
#ifndef IS_PSP
//Old mapping of SDL calls.
#define doSwapLE16(x) SDL_SwapLE16(x)
#define doSwapLE32(x) SDL_SwapLE32(x)
#define doSwapLE64(x) SDL_SwapLE64(x)
#define doSwapBE16(x) SDL_SwapBE16(x)
#define doSwapBE32(x) SDL_SwapBE32(x)
#define doSwapBE64(x) SDL_SwapBE64(x)
#endif
#else
//Compatibility mapping of SDL calls.
#define doSwapLE16(x) SDL_Swap16LE(x)
#define doSwapLE32(x) SDL_Swap32LE(x)
#define doSwapLE64(x) SDL_Swap64LE(x)
#define doSwapBE16(x) SDL_Swap16BE(x)
#define doSwapBE32(x) SDL_Swap32BE(x)
#define doSwapBE64(x) SDL_Swap64BE(x)
#endif

#ifndef SDL3
#ifndef doSwapLE64
//SDL_SwapLE64 when not supported on the platform itself
#ifdef IS_BIG_ENDIAN
#define doSwapLE64(x) (doSwapLE32(((x) >> 32) & 0xFFFFFFFFULL) | (doSwapLE32((x) & 0xFFFFFFFFULL) << 32))
#else
#define doSwapLE64(x) (x)
#endif
#endif
#endif

#ifdef SDL3
#define semaphore_type SDL_Semaphore
#define doSemWait SDL_WaitSemaphore
#if SDL_VERSION_ATLEAST(3,1,2)
#define doSemPost SDL_SignalSemaphore 
#else
#define doSemPost SDL_PostSemaphore
#endif
#else
#define semaphore_type SDL_sem
#define doSemWait SDL_SemWait
#define doSemPost SDL_SemPost
#endif

#endif