/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/support/zalloc.h" //Our own definitions!
#include "headers/support/log.h" //Logging support!
#include "headers/support/locks.h" //Locking support!
#include "headers/emu/sound.h" //For locking the audio thread!

#include <malloc.h> //Specific to us only: the actual memory allocation!

//Simple canary!
#define USECANARIES 0
#define PTRCANARY 0xAABBCCDD

byte allcleared = 0; //Are all pointers cleared?
word maxptrnamelen = 0; //Maximum pointer name length detected?
char maxptrnamelenname[18] = ""; //Longest name detected when allocating!

typedef struct
{
uint_64 size; //Size of the data!
void *pointer; //Pointer to the start of the allocated data!
DEALLOCFUNC dealloc; //Deallocation function!
//Extra optimizations:
ptrnum ptrstart, ptrend; //Start&end address of the pointer!
semaphore_type *thelock; //The lock applied to this pointer: if it's to be freed and this is set, wait for this lock to be free!
//Extra information
char name[18]; //The name of the allocated entry!
byte hascanary; //Do we have a canary?
} POINTERENTRY;

//8192 entries for a small as a memory footprint as possible!
#define NUMPOINTERS 8192

POINTERENTRY registeredpointers[NUMPOINTERS]; //All registered pointers!
POINTERENTRY registeredpointerscopy[NUMPOINTERS]; //All registered pointers for logging!
byte pointersinitialised = 0; //Are the pointers already initialised?
word registeredpointerscount = 0; //How many pointers are registered at all?

//Limit each block allocated to this number when defined! Limit us to 4G for memory on 32-bit platforms, 8G on 64-bit platforms!
#ifdef IS_64BIT
//Supporting 64-bit memory allocations!
#define MEM_BLOCK_LIMIT 0x200000000ULL
#define MEM_ARCH_LIMIT 0xFFFFFFFFFFFFFFFFULL
#else
//Only supporting 32-bit memory allocations on 32-bit platforms!
#define MEM_BLOCK_LIMIT 0xFFFFFFFFULL
#define MEM_ARCH_LIMIT 0xFFFFFFFFULL
#endif

//Debug undefined deallocations?
#define DEBUG_WRONGDEALLOCATION
//Debug allocation and deallocation?
//#define DEBUG_ALLOCDEALLOC

//Pointer registration/unregistration

byte allow_zallocfaillog = 1; //Allow zalloc fail log?

//Initialisation.
OPTINLINE void initZalloc() //Initialises the zalloc subsystem!
{
	lock(LOCK_ZALLOC);
	if (pointersinitialised)
	{
		unlock(LOCK_ZALLOC);
		return;//Don't do anything when we're ready already!
	}
	memset(&registeredpointers,0,sizeof(registeredpointers)); //Initialise all registered pointers!
	atexit(&freezall); //Our cleanup function registered!
	pointersinitialised = 1; //We're ready to run!
	unlock(LOCK_ZALLOC);
}

//Log all pointers to zalloc.
void logpointers(char *cause) //Logs any changes in memory usage!
{
	int current;
	uint_64 total_memory = 0; //For checking total memory count!
	initZalloc(); //Make sure we're started!
	lock(LOCK_ZALLOC);
	memcpy(&registeredpointerscopy, &registeredpointers, sizeof(registeredpointers)); //Create a copy to log!
	unlock(LOCK_ZALLOC);
	dolog("zalloc","Starting dump of allocated pointers (cause: %s)...",cause);
	for (current=0;current<(int)NUMITEMS(registeredpointerscopy);current++)
	{
		if (registeredpointerscopy[current].pointer && registeredpointerscopy[current].size && (registeredpointerscopy[current].hascanary!=0xFF))//Registered?
		{
			if (safestrlen(registeredpointerscopy[current].name,sizeof(registeredpointerscopy[0].name))>0) //Valid name?
			{
				dolog("zalloc","- %s with %u bytes@%p",registeredpointerscopy[current].name,registeredpointerscopy[current].size,registeredpointerscopy[current].pointer); //Add the name!
				total_memory += registeredpointerscopy[current].size; //Add to total memory!
			}
		}
	}
	dolog("zalloc","End dump of allocated pointers.");
	dolog("zalloc","Total memory allocated: %u bytes",total_memory); //We're a full log!
}

//(un)Registration and lookup of pointers.

/*
Matchpointer: matches an pointer to an entry?
parameters:
	ptr: The pointer!
	index: The start index (in bytes)
	size: The size of the data we're going to dereference!
Result:
	-2 when not matched, -1 when matched within another pointer, 0+: the index in the registeredpointers table.
	
*/

OPTINLINE sword matchptr(void *ptr, uint_32 index, uint_64 size, char *name) //Are we already in our list? Give the position!
{
	INLINEREGISTER ptrnum address_start, address_end, currentstart, currentend;
	INLINEREGISTER int left=0;
	if (!ptr) return -2; //Not matched when NULL!
	if (!size) return -2; //Not matched when no size (should be impossible)!

	address_start = (ptrnum)ptr;
	address_start += index; //Start of data!
	address_end = address_start; //Start of data!
	address_end += size; //Add the size!
	--address_end; //End of data!


	for (;left<(int)NUMITEMS(registeredpointers);++left) //Process matchable options!
	{
		currentstart = registeredpointers[left].ptrstart; //Start of addressing!
		if (currentstart==0) continue; //Gotten anything at all?
		currentend = registeredpointers[left].ptrend; //End of addressing!
		if (name)
		{
			if (strcmp(registeredpointers[left].name, name)!=0) continue; //Invalid name? Skip us if so!
		}
		if (currentstart > address_start) continue; //Skip: not our pointer!
		if (currentend < address_end) continue; //Skip: not our pointer!
		//Within range? Give the correct result!
		if (currentstart != address_start) return -1; //Partly match!
		if (currentend != address_end) return -1; //Partly match!
		//We're a full match!
		return (sword)left; //Full match at this index!
	}

	//Compatiblity only!
	return -2; //Not found!
}

byte registerptr(void *ptr,uint_64 size, char *name,DEALLOCFUNC dealloc, semaphore_type *thelock, byte hascanary) //Register a pointer!
{
	uint_32 current; //Current!
	ptrnum ptrend;
	initZalloc(); //Make sure we're started!
	if (!ptr)
	{
		#ifdef DEBUG_ALLOCDEALLOC
		if (allow_zallocfaillog) dolog("zalloc","WARNING: RegisterPointer %s with size %u has invalid pointer!",name,size);
		#endif
		return 0; //Not a pointer?
	}
	if (!size)
	{
		#ifdef DEBUG_ALLOCDEALLOC
		if (allow_zallocfaillog) dolog("zalloc","WARNING: RegisterPointer %s with no size!",name,size);
		#endif
		return 0; //Not a size, so can't be a pointer!
	}
	lock(LOCK_ZALLOC);
	if (matchptr(ptr, 0, size, NULL) > -2)
	{
		unlock(LOCK_ZALLOC);
		return 1;//Already gotten (prevent subs to register after parents)?
	}
	for (current = 0; current < NUMITEMS(registeredpointers); current++)//Process valid!
	{
		if ((!registeredpointers[current].pointer || !registeredpointers[current].size) && (registeredpointers[current].hascanary!=0xFF))//Unused?
		{
			if (registeredpointers[current].pointer == ptr) //Same?
			{
				registeredpointers[current].hascanary = (hascanary==2)?registeredpointers[current].hascanary:hascanary; //The deallocation function to call, has a canary, if any to use!
			}
			else
			{
				registeredpointers[current].hascanary = hascanary; //The deallocation function to call, has a canary, if any to use!
			}
			registeredpointers[current].pointer = ptr; //The pointer!
			registeredpointers[current].size = size; //The size!
			registeredpointers[current].dealloc = dealloc; //The deallocation function to call, if any to use!
			cleardata(&registeredpointers[current].name[0],sizeof(registeredpointers[current].name)); //Initialise the name!
			safestrcpy(registeredpointers[current].name,sizeof(registeredpointers[0].name),name); //Set the name!
			if (unlikely(safestrlen(name, 256) > maxptrnamelen)) //Longer name registered than used?
			{
				if (safestrlen(name, 256) > safestrlen(registeredpointers[current].name, sizeof(registeredpointers[current].name))) //Overflow?
				{
					unlock(LOCK_ZALLOC);
					dolog("zalloc", "Warning: Pointer name overflow: %s", name); //Log the name as being too long!
					lock(LOCK_ZALLOC);
				}
				else //No overflow?
				{
					maxptrnamelen = safestrlen(registeredpointers[current].name, sizeof(registeredpointers[current].name)); //Longest length registered!
					safestrcpy(maxptrnamelenname, sizeof(maxptrnamelenname), name); //Set the name!
				}
			}
			registeredpointers[current].ptrstart = (ptrnum)ptr; //Start of the pointer!
			ptrend = registeredpointers[current].ptrstart;
			ptrend += size; //Add the size!
			--ptrend; //The end of the pointer is before the size!
			registeredpointers[current].ptrend = ptrend; //End address of the pointer for fast checking!
			registeredpointers[current].thelock = thelock; //Register the lock too!
			++current; //One more for the item count!
			if (unlikely(current > registeredpointerscount))
			{
				registeredpointerscount = current; //How many pointers are actually used!
			}
			unlock(LOCK_ZALLOC);
			#ifdef DEBUG_ALLOCDEALLOC
			if (allow_zallocfaillog)
			{
				dolog("zalloc", "Memory has been allocated. Size: %u. name: %s, location: %p", size, name, ptr);//Log our allocated memory!
			}
			#endif
			return 1;//Registered!
		}
	}
	unlock(LOCK_ZALLOC);
#ifndef _DEBUG
	//Only do this when debugging!
	raiseNonFatalError("zalloc","Registration buffer full@%s@%p!",name,ptr);
#endif
	return 0; //Give error!
}

byte unregisterptr(void *ptr, uint_64 size) //Remove pointer from registration (only if original pointer)?
{
	int index;
	initZalloc(); //Make sure we're started!
	lock(LOCK_ZALLOC);
	if ((index = matchptr(ptr,0,size,NULL))>-1) //We've been found fully?
	{
		if ((registeredpointers[index].pointer==ptr) && (registeredpointers[index].size==size) && (registeredpointers->hascanary!=0xFF)) //Fully matched (parents only)?
		{
			if (registeredpointers[index].hascanary == 1) //Do we have a canary to check?
			{
				registeredpointers[index].hascanary = 0xFF; //Invalidating the pointer itself now!
				if (unlikely((*((uint_32*)((byte *)registeredpointers[index].pointer + registeredpointers[index].size))) != PTRCANARY)) //Canary failed?
				{
					//Invalidate first, before logging! Keep it valid in theory, but unallocatable by moving out-of-range.
					registeredpointers[index].hascanary = 0xFF;//Invalidating the pointer itself now!
					unlock(LOCK_ZALLOC);
					dolog("zalloc", "Zalloc canary of %s has triggered! Heap overflow detected!",registeredpointers[index].name);
					lock(LOCK_ZALLOC);
				}
			}
			else
			{
				registeredpointers[index].hascanary = 0xFF;//Invalidating the pointer itself now!
			}
			#ifdef DEBUG_ALLOCDEALLOC
			if (allow_zallocfaillog)
			{
				unlock(LOCK_ZALLOC);
				dolog("zalloc", "Freeing pointer %s with size %u bytes...", registeredpointers[index].name, size);//Show we're freeing this!
				lock(LOCK_ZALLOC);
			}
			#endif
			memset(&registeredpointers[index],0,sizeof(registeredpointers[index])); //Clear the pointer entry to it's defaults!
			unlock(LOCK_ZALLOC);
			return 1; //Safely unregistered!
		}
	}
	unlock(LOCK_ZALLOC);
	return 0; //We could't find the pointer to unregister!
}

byte changedealloc(void *ptr, uint_64 size, DEALLOCFUNC dealloc) //Change the default dealloc func for an entry (used for external overrides)!
{
	int index;
	initZalloc(); //Make sure we're started!
	lock(LOCK_ZALLOC);
	if ((index = matchptr(ptr, 0, size, NULL)) > -1) //We've been found fully?
	{
		registeredpointers[index].dealloc = dealloc; //Set the new deallocation function!
		unlock(LOCK_ZALLOC);
		return 1;//Set!
	}
	unlock(LOCK_ZALLOC);
	return 0;//Not found!
}

//Core allocation/deallocation functions.
void zalloc_free(void **ptr, uint_64 size, semaphore_type *thelock) //Free a pointer (used internally only) allocated with nzalloc/zalloc!
{
	if (thelock)
	{
		WaitSem(thelock) //Wait for the lock!
	}
	void *ptrdata = NULL;
	if (ptr) //Valid pointer to our pointer?
	{
		ptrdata = *ptr; //Read the current pointer!
		if (unregisterptr(ptrdata,size)) //Safe unregister, keeping parents alive, use the copy: the original pointer is destroyed by free in Visual C++?!
		{
			free(ptrdata); //Release the valid allocated pointer!
		}
		*ptr = NULL; //Release the pointer given!
	}
	if (thelock)
	{
		PostSem(thelock) //Release the lock!
	}
}

DEALLOCFUNC getdefaultdealloc()
{
	return &zalloc_free; //Default handler used by us!
}

void *nzalloc(uint_64 size, char *name, semaphore_type *thelock) //Allocates memory, NULL on failure (ran out of memory), protected malloc!
{
	void *ptr;
	initZalloc(); //Make sure we're started!
	if (!size) return NULL; //Can't allocate nothing!
	if ((((size_t)size + sizeof(uint_32))) < (size_t)size) return NULL; //Can't allocate past memory boundaries!
	ptr = malloc((size_t)size+sizeof(uint_32)); //Try to allocate once!

	if (ptr!=NULL) //Allocated and a valid size?
	{
		#if USECANARIES==1
		*((uint_32*)(((byte *)ptr)+size)) = PTRCANARY; //Setup the canary!
		#endif
		if (registerptr(ptr,size,name,getdefaultdealloc(),thelock,USECANARIES)) //Register the pointer with the detection system, using the default dealloc functionality!
		{
			return ptr; //Give the original pointer, cleared to 0!
		}
		#ifdef DEBUG_ALLOCDEALLOC
		if (allow_zallocfaillog) dolog("zalloc","Ran out of registrations while allocating %u bytes of data for block %s.",size,name);
		#endif
		free(ptr); //Free it, can't generate any more!
	}
	#ifdef DEBUG_ALLOCDEALLOC
	else if (allow_zallocfaillog)
	{
		if (freemem()>=size) //Enough memory after all?
		{
			dolog("zalloc","Error while allocating %u bytes of data for block \"%s\" with enough free memory(%i bytes).",size,name,freemem());
		}
		else
		{
			dolog("zalloc","Ran out of memory while allocating %u bytes of data for block \"%s\".",size,name);
		}
	}
	#endif
	return NULL; //Not allocated!
}

//Deallocation core function.
void freez(void **ptr, uint_64 size, char *name)
{
	int ptrn=-1;
	initZalloc(); //Make sure we're started!
	if (!ptr) return; //Invalid pointer to deref!
	lock(LOCK_ZALLOC);
	if ((ptrn = matchptr(*ptr,0,size,NULL))>-1) //Found fully existant?
	{
		if (!registeredpointers[ptrn].dealloc) //Deallocation not registered?
		{
			unlock(LOCK_ZALLOC);
			return; //We can't be freed using this function! We're still allocated!
		}
		unlock(LOCK_ZALLOC);
		registeredpointers[ptrn].dealloc(ptr,size,registeredpointers[ptrn].thelock); //Release the memory tied to it using the registered deallocation function, if any!
	}
	#ifdef DEBUG_ALLOCDEALLOC
	else if (allow_zallocfaillog && ptr!=NULL) //An pointer pointing to nothing?
	{
		unlock(LOCK_ZALLOC);
		dolog("zalloc","Warning: freeing pointer which isn't an allocated reference: %s=%p",name,*ptr); //Log it!
	}
	#endif
	else
	{
		unlock(LOCK_ZALLOC);
	}
	//Still allocated, we might be a pointer which is a subset, so we can't deallocate!
}

//Allocation support: add initialization to zero.
void *zalloc(uint_64 size, char *name, semaphore_type *thelock) //Same as nzalloc, but clears the allocated memory!
{
	void* ptr;
	initZalloc(); //Make sure we're started!
	if (!size) return NULL; //Can't allocate nothing!
	if ((((size_t)size + sizeof(uint_32))) < (size_t)size) return NULL; //Can't allocate past memory boundaries!
	ptr = calloc(1,(size_t)size + sizeof(uint_32)); //Try to allocate once!

	if (ptr != NULL) //Allocated and a valid size?
	{
		#if USECANARIES==1
		* ((uint_32*)(((byte*)ptr) + size)) = PTRCANARY; //Setup the canary!
		#endif
		if (registerptr(ptr, size, name, getdefaultdealloc(), thelock, USECANARIES)) //Register the pointer with the detection system, using the default dealloc functionality!
		{
			return ptr; //Give the original pointer, cleared to 0!
		}
		#ifdef DEBUG_ALLOCDEALLOC
		if (allow_zallocfaillog) dolog("zalloc", "Ran out of registrations while allocating %u bytes of data for block %s.", size, name);
		#endif
		free(ptr); //Free it, can't generate any more!
	}
	#ifdef DEBUG_ALLOCDEALLOC
	else if (allow_zallocfaillog)
	{
		if (freemem() >= size) //Enough memory after all?
		{
			dolog("zalloc", "Error while allocating %u bytes of data for block \"%s\" with enough free memory(%i bytes).", size, name, freemem());
		}
		else
		{
			dolog("zalloc", "Ran out of memory while allocating %u bytes of data for block \"%s\".", size, name);
		}
	}
	#endif
	return NULL; //Not allocated!
}

//Deallocation support: release all registered pointers! This used to be unregisterptrall.
void freezall(void) //Free all allocated memory still allocated (on shutdown only, garbage collector)!
{
	int i;
	void *ptr;
	uint64_t size;
	initZalloc(); //Make sure we're started!
	#ifdef SDL3
	if (SDL_WasInit(SDL_INIT_AUDIO)) lockaudio(); //Do make sure the SDL audio callback isn't running!
	#else
	if (SDL_WasInit(SDL_INIT_AUDIO)) SDL_LockAudio(); //Do make sure the SDL audio callback isn't running!
	#endif
	lock(LOCK_MAINTHREAD); //Make sure we're not running!
	allcleared = 1; //All is cleared!
	lock(LOCK_ZALLOC);
	for (i=0;i<(int)NUMITEMS(registeredpointers);i++)
	{
		ptr = registeredpointers[i].pointer;
		size = registeredpointers[i].size;
		unlock(LOCK_ZALLOC);
		freez(&ptr,size,"Unregisterptrall"); //Unregister a pointer when allowed!
		lock(LOCK_ZALLOC);
	}
	unlock(LOCK_ZALLOC);
	#ifdef SDL3
	if (SDL_WasInit(SDL_INIT_AUDIO)) unlockaudio(); //We're done with audio processing!
	#else
	if (SDL_WasInit(SDL_INIT_AUDIO)) SDL_UnlockAudio(); //We're done with audio processing!
	#endif
	unlock(LOCK_MAINTHREAD); //Finished!
}

//Memory protection/verification function. Returns the pointer when valid, NULL on invalid.
void *memprotect(void *ptr, uint_64 size, char *name) //Checks address of pointer!
{
	if ((!ptr) || (ptr==NULL)) //Invalid?
	{
		return NULL; //Invalid!
	}
	initZalloc(); //Make sure we're started!
	if (matchptr(ptr, 0, size, name) > -2)//Pointer matched (partly or fully)?
	{
		return ptr; //Give the pointer!
	}
	return NULL; //Invalid!
}

//Detect free memory.
uint_64 freemem() //Largest Free memory block left to allocate!
{
	uint_64 curalloc; //Current allocated memory! This is the result to give!
	char *buffer=NULL;
	uint_64 multiplier; //The multiplier!
	uint_64 lastzalloc = 0;

	curalloc = 0; //Reset at 1 bytes!
	multiplier = (1ULL<<((sizeof(curalloc)*8)-1)); //Start at max multiplier bit!

	for (;;) //While not done...
	{
		lastzalloc = (curalloc|multiplier); //Last zalloc!
		if (((uint_64)(lastzalloc+sizeof(uint_32))<=MEM_ARCH_LIMIT) && (((size_t)(lastzalloc+sizeof(uint_32)))>=lastzalloc) && (lastzalloc<=MEM_BLOCK_LIMIT)) //Within memory range and not wrapping?
		{
			buffer = (char *)malloc((size_t)(lastzalloc+sizeof(uint_32))); //Try allocating, don't have to be cleared!
			if (buffer) //Allocated?
			{
				free(buffer); //Release memory for next try!
				curalloc = (uint_64)lastzalloc; //Set detected memory!
			}
		}
		multiplier >>= 1; //Shift to the next bit position to check!
		if (multiplier==0) //Gotten an allocation and/or done?
		{
			break; //Stop searching: we're done!
		}
		//We're to continue!
	} //We have success!
	
	if (curalloc > MEM_BLOCK_LIMIT) //More than the limit?
	{
		curalloc = MEM_BLOCK_LIMIT; //Limit to this much!
	}
	if (curalloc > MEM_ARCH_LIMIT) //More than the limit?
	{
		curalloc = MEM_ARCH_LIMIT; //Limit to this much!
	}
	return curalloc; //Give free allocatable memory size!
}
