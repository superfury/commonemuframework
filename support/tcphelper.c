/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic type support!
#include "headers/support/log.h" //Logging support!
#include "headers/support/tcphelper.h" //TCP module support!

#if defined(IS_PSP) || defined(IS_VITA) || defined(IS_SWITCH) || defined(IS_PS3)
#include "headers/support/locks.h" //Locking support!
#endif

#ifdef IS_PSP
#include "headers/emu/threads.h" //Thread support!
#include "headers/emu/input.h" //Input support!
#include "headers/emu/gpu/gpu_text.h" //Text display support!

//Now, for any remaining network support on the PSP!
#include <pspsdk.h>
#include <pspwlan.h> //WLAN support!
#include <psppower.h>
#include <pspnet_apctl.h> //AP control support
#include <psputility_netmodules.h> //Module loading support for net modules!
#include <psputility_netparam.h> //Net param support
#endif

#ifdef IS_VITA
#ifdef GOTNET
#include <psp2/sysmodule.h> //For loading 
#include <psp2/net/net.h> //For net support!
#include <psp2/net/netctl.h> //For netctl support!
#endif
#endif

#define NET_LOGFILE "net"

#if defined(SDL_NET) || defined(SDL2_NET) || defined(SDL3_NET)
#if defined(SDL3) && defined(SDL2_NET)
#define WITHOUT_SDL
#if defined(IS_VITA) || defined(IS_SWITCH)
#include <SDL2/SDL_net.h> //SDL3 NET support!
#else
#include "SDL_net.h" //SDL2 NET support!
#endif
#else
#if defined(SDL3) && defined(SDL3_NET)
//#if defined(IS_VITA) || defined(IS_SWITCH)
#include <SDL3_net/SDL_net.h> //SDL3 NET support!
//#else
//#include "SDL_net.h" //SDL2 NET support!
//#ifdef GOTNET
//Not supported in this case!
//#undef GOTNOT
//#endif
#else
#if defined(SDL2) && defined(SDL2_NET)
#if defined(IS_VITA) || defined(IS_SWITCH) || defined(IS_PS3)
#include <SDL2/SDL_net.h> //SDL2 NET support!
#else
#include "SDL_net.h" //SDL2 NET support!
#endif
#else
#ifdef SDL_NET
#if defined(IS_VITA)
#include <SDL/SDL_net.h> //SDL NET support!
#else
#if defined(IS_PS3)
#include <SDL/SDL_net.h>
#else
#include "SDL_net.h"
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#define TCPMENU_INPUTDELAY 250000

#ifdef GOTNET
#ifndef SDL3_NET
#include "headers/emu/threads.h" //Threading support for resolving IP addresses and async connecting!
#endif
#endif

/*
#if defined(IS_PSP) && defined(GOTNET)
//Test: run without SDL_net, but with networking installed!
#undef GOTNET
//Run networking PSP-specific base support anyways!
#define PSP_DISABLEDNET
#endif
*/

//Some server port to use when listening and sending data packets.
#ifdef GOTNET
byte allocatedconnections[0x101]; //Connection 0 is reserved!
#if defined(SDL2_NET) || defined(SDL_NET)
TCPsocket server_socket;
TCPsocket mysock[0x101]; //Connection 0 is reserved!
IPaddress remoteaddr[0x101]; //Connection 0 is reserved!
IPaddress client_openip[0x101]; //IP field for resolving.
char client_destination[0x101][256];
word client_destinationport[0x101];
word client_connectthread_id[0x101]; //ID field for passing to the thread!
ThreadParams_p client_connectthread[0x101]; //A thread that's running for the client connection!
byte client_connectthread_result; //The current result of the connect thread!
SDLNet_SocketSet listensocketset[0x101]; //Socket set of a connection for listening!
SDLNet_SocketSet serverlistensocketset; //Socket set of a connection for listening!
#else
//SDL3_net?
SDLNet_Server* server_socket;
SDLNet_StreamSocket *mysock[0x101]; //Connection 0 is reserved!
SDLNet_Address *client_openip[0x101]; //IP resolution
word client_openport[0x101]; //IP connecting port for connections made!
typedef struct
{
	uint_32 host;
	word V6addr[8]; //Full V6 address!
	word port;
	byte protocol; //0=IPv5, 1=IPv6, 2=Unknown protocol?
} IPaddress;
IPaddress remoteaddr[0x101]; //Connection 0 is reserved!
void *listensocketset[0x101]; //Socket set of a connection for listening!
void *serverlistensocketset; //Socket set of a connection for listening!
#endif
byte Client_READY[0x100]; //Client ready for use?
byte splitclientserverconnections = 0; //Split client/server connections?
word availableclientconnections = 1; //Available connections(one less than can be allocated(reserved connection), because of sending connection(#0))?
word availableserverconnections = 1; //Available connections(one less than can be allocated(reserved connection), because of sending connection(#0))?
word SERVER_PORT = 23; //What server port to apply?

word* getclientserverconnection(byte isclient)
{
	if (splitclientserverconnections == 0)
	{
		return &availableclientconnections; //Use client connections only!
	}
	if (isclient) //For client?
	{
		return &availableclientconnections; //Client connection!
	}
	return &availableserverconnections; //Server connection!
}

word freeclientserverconnections()
{
	if (splitclientserverconnections == 0)
	{
		return availableclientconnections; //Client only!
	}
	//Server only!
	return availableserverconnections; //Server connections available when split!
}
#endif

byte NET_READY = 0; //Are we ready to be used?
byte Server_READY = 0; //Server ready for use?
byte server_preventselfconnection = 1; //Prevent connecting to ourselves when connecting to a client?
byte wifi_connected = 1; //Default: connected to WiFi not handled by us, so always connected!

#if defined(GOTNET) || defined(PSP_DISABLEDNET)
#ifdef IS_PSP
byte PSP_netmodules_loaded = 0; //Default: no net modules loaded!
#endif
#ifdef IS_VITA
byte PSV_netmodules_loaded = 0; //Default: no net modules loaded!
#endif
#ifdef IS_SWITCH
byte SWITCH_netmodules_loaded = 0; //Default: not loaded!
#endif
#endif

void initTCP() //Initialize us!
{
#if defined(GOTNET) || defined(PSP_DISABLEDNET)
	atexit(&doneTCP); //Automatically terminate us when used!
#ifdef IS_PSP
	wifi_connected = 0; //Default: not connected to any WiFi!
	int err;

	PSP_netmodules_loaded = 0; //Default: none loaded yet!
	err = sceUtilityLoadNetModule(PSP_NET_MODULE_COMMON);
	if (err != 0)
	{
		dolog(NET_LOGFILE,"Error, could not load PSP_NET_MODULE_COMMON %08X\n", err);
		NET_READY = 0; //Not ready!
		return;
	}
	PSP_netmodules_loaded |= 1; //Loaded common!
	err = sceUtilityLoadNetModule(PSP_NET_MODULE_INET);
	if (err != 0)
	{
		dolog(NET_LOGFILE,"Error, could not load PSP_NET_MODULE_INET %08X\n", err);
		NET_READY = 0; //Not ready!
		return;
	}
	PSP_netmodules_loaded |= 2; //Loaded inet!
#endif
#ifdef IS_VITA
	int err;
	PSV_netmodules_loaded = 0; //Default: none loaded yet!
	if ((err = sceSysmoduleLoadModule(SCE_SYSMODULE_NET)) != 0) //Failed to load?
	{
		dolog(NET_LOGFILE, "Error, could not load SCE_SYSMODULE_NET %08X\n", err);
		NET_READY = 0; //Not ready!
	}
	PSV_netmodules_loaded |= 1; //Loaded SCE_SYSMODULE_NET!

	SceNetInitParam netInitParam;
	int size = 4 * 1024 * 1024;
	netInitParam.memory = malloc(size);
	netInitParam.size = size;
	netInitParam.flags = 0;
	if ((err = sceNetInit(&netInitParam)) != 0)
	{
		dolog(NET_LOGFILE, "Error, could not init Net %08X\n", err);
		NET_READY = 0; //Not ready!
	}
	PSV_netmodules_loaded |= 2; //Step 2!

	if ((err = sceNetCtlInit()) != 0)
	{
		dolog(NET_LOGFILE, "Error, could not init NetCtl %08X\n", err);
		NET_READY = 0; //Not ready!
	}
	PSV_netmodules_loaded |= 4; //Step 3!
#endif
#ifdef IS_SWITCH
	uint_32 err;
	SWITCH_netmodules_loaded = 0; //Default: none loaded yet!
	if ((err = socketInitializeDefault())!=0) //Initialize sockets!
	{
		dolog(NET_LOGFILE, "Error, could not init sockets %08X\n", err);
		NET_READY = 0; //Not ready!
	}
	SWITCH_netmodules_loaded |= 1; //Initialized!
#endif
#ifdef GOTNET
#if defined(SDL_NET) || defined(SDL2) || defined(SDL3)
	#ifdef SDL3
	int initresult;
	#ifdef SDL_INIT_TIMER
	initresult = SDL_Init(SDL_INIT_TIMER | SDL_INIT_EVENTS);
	#else
	initresult = SDL_Init(SDL_INIT_EVENTS);
	#endif
	#if SDL_VERSION_ATLEAST(3,1,2)
	initresult = !initresult; //Get False for success, true for failure!
	#else
	initresult = (initresult<0); //Negative for failure
	#endif
	if (initresult) { //Errored out?
	#else
	if (SDL_Init(SDL_INIT_TIMER) < 0) {
	#endif
		dolog(NET_LOGFILE, "ER: SDL_Init: %s\n", SDL_GetError());
		NET_READY  = 0; //Not ready!
		return;
	}
 
	#if SDL_VERSION_ATLEAST(3, 1, 2)
	if (!SDLNet_Init()) {
	#else
	if(SDLNet_Init() == -1) {
	#endif
		#ifndef SDL3_NET
		dolog(NET_LOGFILE, "ER: SDLNet_Init: %s\n", SDLNet_GetError());
		#else
		dolog(NET_LOGFILE, "ER: SDLNet_Init: %s\n", SDL_GetError());
		#endif
		NET_READY = 0; //Not ready!
		return;
	}
	memset(&allocatedconnections, 0, sizeof(allocatedconnections)); //Save if allocated or not!
	memset(&Client_READY, 0, sizeof(Client_READY)); //Init client ready status!
	availableclientconnections =  1; //init!
	availableserverconnections = 0; //Init!
	NET_READY = 1; //NET is ready!
#endif
#endif
	#endif
	//Initialize buffers for the server, as well as for the client, if used!
}

void TCPServer_INTERNAL_startserver();
void TCPServer_INTERNAL_stopserver(byte fullstop);

byte TCP_ConnectServer(word port, word numConnections)
{
#ifdef GOTNET
	if (Server_READY) return 1; //Already started!
	Server_READY = 0; //Not ready by default!
	if (!NET_READY)
	{
		return 0; //Fail automatically!
	}
	if (!numConnections) //No connections to allocate?
	{
		return 0; //Fail automatically!
	}
	availableserverconnections = MIN(numConnections,NUMITEMS(allocatedconnections)-1); //How many to allocate maximum! first is reserved to connect for a client!

	SERVER_PORT = port; //Set the port to use!

	//Clear the server I/O buffers!
	return Server_READY; //Connected!
#else
return 0; //Cannot connect!
#endif
}
#ifdef GOTNET
sword allocTCPid(byte isClient) //Allocate an connection to use!
{
	byte *connection, *endconnection, *clientconnection;
	sword connectionid;
	if (!*getclientserverconnection(isClient)) return -1; //Couldn't allocate!
	--*getclientserverconnection(isClient); //Indicate we're allocating one!
	connection = &allocatedconnections[0]; //Check them all!
	clientconnection = connection; //Reserved client connection!
	endconnection = &allocatedconnections[NUMITEMS(allocatedconnections)]; //Where to end!
	connectionid = 0; //Init ID!
	for (; (connection != endconnection);) //Check them all!
	{
		if ((isClient) || ((connection != clientconnection) || (!splitclientserverconnections))) //Not reserved client connection or client?
		{
			if (!*connection) //Not allocated?
			{
				*connection = (isClient ? 1 : 2); //Allocated to use for client or server!
				return connectionid; //Give the ID(base 0)!
			}
		}
		++connection; //Next connection!
		++connectionid; //Next ID!
		if (isClient)
		{
			++*getclientserverconnection(isClient); //Restore!
			return -1; //Client connection is reserved #0 only, so can't allocate!
		}
	}
	++*getclientserverconnection(isClient); //One more available again!
	return -1; //Couldn't find any!
}

byte freeTCPid(sword id) //Free a connection to use!
{
	byte isserver;
	if (id < 0) return 0; //Can't release when not allocated!
	if ((availableclientconnections+availableserverconnections) >= NUMITEMS(allocatedconnections)) return 0; //Couldn't release: nothing allocated!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (allocatedconnections[id]) //Allocated?
	{
		#ifdef GOTNET
		#ifndef SDL3_NET
		if (client_connectthread[id]) //Gotten a connection thread?
		{
			waitThreadEnd(client_connectthread[id]); //Wait for it to finish.
			client_connectthread[id] = NULL; //Make sure to finish properly!
		}
		Client_READY[id] = 0; //Not ready!
		#endif
		#endif
		isserver = (allocatedconnections[id] == 2); //Server connection?
		allocatedconnections[id] = 0; //Free!
		++*getclientserverconnection(isserver); //One more available!
		return 1; //Deallocated!
	}
	return 0; //Not found!
}

byte decodeIPv6(char* ipv6, word* decoded, word *port)
{
	byte gottenaddr;
	word* decodepos, *splitdecodepos;
	byte splitpos;
	byte finalindex; //Final index for split parsing!
	byte inputleft;
	char *inputpos; //Input position we're reading at!
	byte inputindex;
	unsigned int dataread;
	word* originaldecoded;
	inputleft = 8; //Maximum of 8 inputs!
	decodepos = originaldecoded = decoded; //First entry to decode to start at and save a backup for post-processing!
	inputpos = ipv6; //Start at the start of the string to decode!
	splitpos = 8; //Default: no split position!
	inputindex = 0; //Start position where we're at!
	*port = 0; //Default port: port unknown!
	gottenaddr = 0; //Default: not gotten an address block!
	for (; (inputleft && *inputpos);) //Any input left?
	{
		if ((*inputpos == '[') && (!inputindex)) //Start address escapsulation identifier?
		{
			gottenaddr = 1; //Address encapsulation started!
		}
		else if (sscanf(inputpos, "::%04x", &dataread)) //Leading zero input?
		{
			if (splitpos == 8) //Nothing yet?
			{
				splitpos = inputindex; //Where ther split happens!
			}
			else //Multiple splits aren't allowed?
			{
				return 0; //Multiple splits are invalid syntax!
			}
			*(decodepos++) = dataread; //The word that was read!
			inputpos += 2; //Skip leading seperator!
			--inputleft; //One less left!
			++inputindex; //Increase the index: 1 read!
			for (; (*inputpos != ':') && (*inputpos);) //Not end of string or next group?
			{
				++inputpos; //Skip!
			}
		}
		else if (sscanf(inputpos, "]:%04x", &dataread) && gottenaddr) //Final port input?
		{
			inputpos += 2; //Skip the header to the address part!
			for (; (*inputpos != ':') && (*inputpos);) //Not end of string or next group?
			{
				++inputpos; //Skip!
			}
			if (*inputpos == ':') //Invalid termination for a port?
			{
				return 0; //Invalid address termination!
			}
			//Proper port termination!
			++inputindex;
			++decodepos;
			//Now we're finished, store the port number now!
			*port = dataread; //The port number that was read!
		}
		else if (sscanf(inputpos, ":%04x", &dataread)) //Normal input?
		{
			*(decodepos++) = dataread; //What was read!
			inputpos += 1; //Skip leading seperator!
			--inputleft; //One less left!
			++inputindex; //Increase the index: 1 read!
			for (; (*inputpos != ':') && (*inputpos);) //Not end of string or next group?
			{
				++inputpos; //Skip!
			}
		}
		else if (sscanf(inputpos, "%04x:", &dataread)) //First input?
		{
			if (inputindex) //Past the first entry? Invalid input!
			{
				return 0; //We can only start with a digit on the first entry!
			}
			*(decodepos++) = dataread; //What was read!
			--inputleft; //One less left!
			++inputindex; //Increase the index: 1 read!
			for (; (*inputpos != ':') && (*inputpos);) //Not end of string or next group?
			{
				++inputpos; //Skip!
			}
		}
		else //Invalid input?
		{
			return 0; //Decoding failed!
		}
	}
	--inputindex; //Point the input index at the final entry!
	--decodepos; //Point the decoder at the final entry we've read!

	//Now, we have the entire ipv6 string decoded, with an optional split position applied, us pointing to the final entry.
	if (splitpos != 8) //Gotten a split to apply?
	{
		if (splitpos == 7) //The split got applied to the final entry? Don't have to do anything!
		{
			return 1; //Decoding success!
		}
		finalindex = inputindex; //Where the split ends!
		//First, move the post-split to the end of the indexes!
		//Reuse the decode position to fill it's own gaps: it's already pointing past the final entry!
		splitdecodepos = &decoded[7]; //Where to start writing the final split fields: at the end of the decoded data!
		for (inputindex = finalindex; ((inputindex>=(finalindex-(finalindex-splitpos))) && (inputindex!=(byte)~0));) //Parse the fields from the final position until and including the split start position!
		{
			*(splitdecodepos--) = *(decodepos--); //Copy the data to the tail!
			--inputindex;
		}
		inputindex = 6 - (finalindex - splitpos); //We're now at the split entry position, so properly indicate that!
		//Now, the input index points to the split's position, the input index is at the point before the split data, the decoded is at the 'split-size'th entry before the end.
		//Fill in the remaining data with zeroes!
		for (; ((inputindex >= splitpos) && (inputindex!=(byte)~0));)
		{
			*(splitdecodepos--) = 0; //Zeroed fill!
			--inputindex;
		}
		//Decoding success!
	}
	//No split to apply? Decoded!
	return 1; //Decoding success!
}

#ifndef SDL3_NET
sword TCP_connectClientFromServer(sword id, TCPsocket source, TCPCLIENTVALIDATIONFUNC validationfunc, TCPCLIENTVALIDATIONV6FUNC validationV6func)
#else
sword TCP_connectClientFromServer(sword id, SDLNet_StreamSocket *source, TCPCLIENTVALIDATIONFUNC validationfunc, TCPCLIENTVALIDATIONV6FUNC validationV6func)
#endif
{
	uint_32 remotehost;
	word remoteport;
	#ifndef SDL3_NET
	IPaddress* remoteip;
	#else
	SDLNet_Address* remoteip;
	int a1, a2, a3, a4; //Picked apart address!
	uint_32 a5;
	char* addrstr;
	word remotehostV6[8]; //Remote host!
	#endif
	//Accept a client as a new server?
	Client_READY[id] = 0; //Init ready status!
	
	mysock[id]=0;
	listensocketset[id]=0;
	if(source!=0) {
		mysock[id] = source;
		#ifndef SDL3_NET
		if ((remoteip = SDLNet_TCP_GetPeerAddress(source))!=NULL)
		{
			memcpy(&remoteaddr[id], remoteip, sizeof(*remoteip)); //Remote address!
		}
		else
		{
			memset(&remoteaddr[id], 0, sizeof(remoteaddr[id])); //Remote address!
		}
		#else
		if ((remoteip = SDLNet_GetStreamSocketAddress(source)) != NULL)
		{
			memset(&remoteaddr[id], 0, sizeof(remoteaddr[id])); //Remote address defaults to not set!
			addrstr = (char *)SDLNet_GetAddressString(remoteip); //Get the address string!
			//Decode IPv4/IPv4 address and port(if available)
			if (sscanf(addrstr, "%i.%i.%i.%i:%" SPRINTF_u_UINT32, &a1, &a2, &a3, &a4, &a5) == 5) //Get the remote address!
			{
				if (a5 > 0xFFFFU) //Too large?
				{
					a5 = 0; //Invalid port!
				}
				remoteaddr[id].protocol = 0; //IPv4!
				remoteaddr[id].host = doSwapBE32((((((a4 << 8) | a3) << 8) | a2) << 8) | a1); //Correct endian address!
				remoteaddr[id].port = doSwapBE16(a5); //The port!
			}
			else if (sscanf("%i.%i.%i.%i", addrstr, &a1, &a2, &a3, &a4) == 4) //Get the remote address!
			{
				remoteaddr[id].protocol = 0; //IPv4!
				remoteaddr[id].host = doSwapBE32((((((a4 << 8) | a3) << 8) | a2) << 8) | a1); //Correct endian address!
				remoteaddr[id].port = 0; //Unknown port!
			}
			else if (decodeIPv6(addrstr, &remoteaddr[id].V6addr[0],&remoteaddr[id].port)) //IPv6?
			{
				remoteaddr[id].port = doSwapBE16(remoteaddr[id].port); //Keep compatibility on the port number!
				remoteaddr[id].protocol = 1; //V6 port!
			}
			else
			{
				remoteaddr[id].protocol = 2; //Unknown!
			}
			SDLNet_UnrefAddress(remoteip); //Done with it!
		}
		else
		{
			memset(&remoteaddr[id], 0, sizeof(remoteaddr[id])); //Remote address!
		}
		#endif
		//First, validation (IPv4)
		#ifndef SDL3_NET
		//SDL1/2 NET
		if (validationfunc) //To validate the client first (IPv4)?
		#else
		//SDL3_NET
		if (validationfunc && (!remoteaddr[id].protocol)) //To validate the client first (IPv4)?
		#endif
		{
			memset(&remotehost, 0, sizeof(remotehost)); //init!
			memset(&remoteport, 0, sizeof(remoteport)); //init!
			memcpy(&remotehost, &remoteaddr[id].host, MIN(sizeof(remotehost),sizeof(remoteaddr[id].host))); //Remote Host!
			memcpy(&remoteport, &remoteaddr[id].port, MIN(sizeof(remoteport),sizeof(remoteaddr[id].port))); //Remote Port!
			if (!validationfunc(remotehost, doSwapBE16(remoteport))) //Validate the client first!
			{
				#ifndef SDL3_NET
				//SDL1/2 NET
				SDLNet_TCP_Close(mysock[id]); //Close the TCP connection!
				#else
				SDLNet_DestroyStreamSocket(source); //Disconnect the connected client!
				#endif
				mysock[id] = NULL; //Deallocated!
				goto finishupsock_server; //Clean up and error out!
			}
		}
		//Next, IPv6 validation (if supported)
		#ifdef SDL3_NET
		if (validationV6func && (remoteaddr[id].protocol==1)) //To validate the client first (IPv6)?
		{
			memset(&remotehost, 0, sizeof(remotehost)); //init!
			memset(&remoteport, 0, sizeof(remoteport)); //init!
			memcpy(&remotehostV6, &remoteaddr[id].V6addr, MIN(sizeof(remotehostV6), sizeof(remoteaddr[id].V6addr))); //Remote Host!
			memcpy(&remoteport, &remoteaddr[id].port, MIN(sizeof(remoteport), sizeof(remoteaddr[id].port))); //Remote Port!
			if (!validationV6func(&remotehostV6[0], doSwapBE16(remoteport))) //Validate the client first!
			{
				SDLNet_DestroyStreamSocket(source); //Disconnect the connected client!
				mysock[id] = NULL; //Deallocated!
				goto finishupsock_server; //Clean up and error out!
			}
		}
		#endif
		#ifndef SDL3_NET
		listensocketset[id] = SDLNet_AllocSocketSet(1);
		if (!listensocketset[id])
		{
			freeTCPid(id); //Free the connection!
			mysock[id] = NULL; //Deallocated!
			return -1;
		}
		if (SDLNet_TCP_AddSocket(listensocketset[id], source) != -1)
		{
		#else
			listensocketset[id] = mysock[id]; //Same, just as an array version!
		#endif
			Client_READY[id] = 2; //Connected as a server!
			if (freeclientserverconnections() == 0) TCPServer_INTERNAL_stopserver(0); //Stop serving if no connections left!
			return id; //Successfully connected!
		#ifndef SDL3_NET
		}
		//Free resources!
		SDLNet_TCP_Close(mysock[id]); //Close the TCP connection!
		mysock[id] = NULL; //Deallocated!
		SDLNet_FreeSocketSet(listensocketset[id]); //Free the socket set allocated!
		listensocketset[id] = NULL; //Deallocated!
		#endif
	}
	finishupsock_server:
	freeTCPid(id); //Free the allocated ID!
	return -1; //Accepting calls aren't supported yet!
}
#endif

byte TCPserverincoming() //Is anything incoming?
{
#ifdef GOTNET
	if (NET_READY == 0) return 0; //Not ready!
	TCPServer_INTERNAL_startserver(); //Start the server unconditionally, if required!
	if (Server_READY != 1) return 0; //Server not running? Not ready!
#ifndef SDL3_NET
	if (!SDLNet_CheckSockets(serverlistensocketset, 0))
	{
		return 0; //Nothing to connect!
	}
#else
	if (SDLNet_WaitUntilInputAvailable(&serverlistensocketset, 1, 0)) //Any pending?
	{
		return 1; //Incoming connection!
	}
	return 0; //Nothing to connect!
#endif

	return 1; //Incoming connection!
#endif
	return 0; //Not supported!
}

sword acceptTCPServer(TCPCLIENTVALIDATIONFUNC validationfunc, TCPCLIENTVALIDATIONV6FUNC validationV6func) //Update anything needed on the TCP server!
{
#ifdef GOTNET
	sword TCPid;
	if (NET_READY==0) return -1; //Not ready!
	TCPServer_INTERNAL_startserver(); //Start the server unconditionally, if required!
	if (Server_READY != 1) return -1; //Server not running? Not ready!

	#ifndef SDL3_NET
	//SDL1/2 NET
	TCPsocket new_tcpsock;
	if (!SDLNet_CheckSockets(serverlistensocketset, 0))
	{
		return -1; //Nothing to connect!
	}
	new_tcpsock = SDLNet_TCP_Accept(server_socket);
	#else
	SDLNet_StreamSocket* new_tcpsock;
	if (!SDLNet_WaitUntilInputAvailable(&serverlistensocketset,1, 0))
	{
		return -1; //Nothing to connect!
	}
	new_tcpsock = NULL; //Init!
	if (SDLNet_AcceptClient(server_socket, &new_tcpsock))
	{
		return -1; //Nothing to connect!
	}
	#endif

	if(!new_tcpsock) {
		return -1; //Nothing to connect!
	}

	if ((TCPid = allocTCPid(0)) < 0) //Couldn't allocate TCP ID!
	{
		#ifndef SDL3_NET
		//SDL1/2 NET
		SDLNet_TCP_Close(new_tcpsock); //Disconnect the connected client!
		#else
		SDLNet_DestroyStreamSocket(new_tcpsock); //Disconnect the connected client!
		#endif
		return -1; //Abort: Nothing to connect!
	}

	return TCP_connectClientFromServer(TCPid,new_tcpsock, validationfunc, validationV6func); //Accept as a client!
#endif
	return -1; //Not supported!
}

byte TCPServerRunning()
{
	return Server_READY; //Is the server running?
}

void stopTCPServer()
{
#ifdef GOTNET
	TCPServer_INTERNAL_stopserver(1); //Perform a full stop!
	Server_READY = 0; //Not ready anymore!
#endif
}

#ifdef GOTNET
#ifndef SDL3_NET
//SDL 1/2 async resolve/connect functionality.
//Async resolve
void TCP_resolveThread()
{
	sword id;
	id = *((sword *)getthreadparams()); //Get our id from our parameter!
	//SDL1/2 NET
	client_connectthread_result = 0; //Default: failed!
	if (!SDLNet_ResolveHost(&client_openip[id], client_destination[id],client_destinationport[id])) //Resolved?
	{
		listensocketset[id] = SDLNet_AllocSocketSet(1);
		if (!listensocketset[id])
		{
			client_connectthread_result = 0; //Failed!
		}
		else //Success?
		{
			client_connectthread_result = 1; //Success!
		}
	}
}

//Async connect
void TCP_connectThread()
{
	sword id;
	id = *((sword *)getthreadparams()); //Get our id from our parameter!
	mysock[id] = SDLNet_TCP_Open(&client_openip[id]);
	client_connectthread_result = 0; //Default result!
	if (!mysock[id])
	{
		client_connectthread_result = 0; //Failed open!
		return;
	}
	memcpy(&remoteaddr[id], &client_openip[id],sizeof(client_openip[id])); //Remote address!
	if (SDLNet_TCP_AddSocket(listensocketset[id], mysock[id]) != -1)
	{
		client_connectthread_result = 1; //Success!
	}
	else
	{
		client_connectthread_result = 2; //Failed socket!
	}
}
#endif
#endif

sword TCP_ConnectClient(const char *destination, word port)
{
#ifdef GOTNET
	byte TCP_Serving;
	sword id;
	if ((id = allocTCPid(1))<0) return -1; //Invalid ID allocated?
	if (Client_READY[id]) //Already connected?
	{
		return -1; //Can't connect: already connected!
	}
	TCP_Serving = Server_READY; //Were we serving?
	if ((TCP_Serving==1) && (server_preventselfconnection)) //Is the server running? We need to stop it to prevent connecting to ourselves!
	{
		TCP_DisconnectClientServer(0); //Disconnect if connected already!
		TCPServer_INTERNAL_stopserver(0); //Stop the server to prevent connections to us! 
	}
	//Ancient versions of SDL_net had this as char*. People still appear to be using this one.
	#ifndef SDL3_NET
	client_connectthread_id[id] = id; //Store the ID for identification to the thread!
	client_destination[id][0] = 0; //Init destination field fast!
	safestrcpy(client_destination[id],sizeof(client_destination[id]),destination); //Save the destination!
	client_destinationport[id] = port; //The port to open!
	client_connectthread[id] = startThread(&TCP_resolveThread, "TCP_resolver",&client_connectthread_id[id]);
	if (client_connectthread[id]) //Success on the first thread?
	{
		Client_READY[id] = 3; //Starting to resolve client!
		return id; //Give the ID of the connecting connection! Client is to call TCP_connected for connection status!
	}
	#else
	//SDL3_NET
	client_openip[id] = SDLNet_ResolveHostname(destination); //Resolve this host!
	if (!client_openip[id]) //Failed?
	{
		//Failed to resolve?
		freeTCPid(id); //Free the used ID!
		return -1; //Failed to connect!
	}
	client_openport[id] = port; //Register the port to open when connected!
	Client_READY[id] = 3; //Starting to resolve client!
	return id; //Give the ID of the connecting connection! Client is to call TCP_connected for connection status!
	#endif
	freeTCPid(id); //Free the used ID!
	return -1; //Failed to connect!
#endif
	return -1; //Not supported!
}

byte TCP_getremote(sword id, uint_32* ipaddr, word* port)
{
#ifdef GOTNET
	IPaddress noipaddr;
	word theport;
	if (id < 0) return 0; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (!allocatedconnections[id]) return 0; //Not allocated!
	if ((!Client_READY[id]) || (Client_READY[id] > 2)) return 0; //Not connected?
	#ifdef SDL3_NET
	if (remoteaddr[id].protocol) //No IPv4 address available?
	{
		return 0; //IPv4 Protocol not supported!
	}
	#endif
	memset(&noipaddr, 0, sizeof(noipaddr)); //No IP address to compare to?
	if (memcmp(&remoteaddr[id], &noipaddr, sizeof(noipaddr))==0) //No IP address found?
	{
		return 0; //Not supported!
	}
	memcpy(ipaddr, &remoteaddr[id].host, 4); //IP address, unchanged (normal IP address)!
	theport = doSwapBE16(remoteaddr[id].port); //The port, converted to host ordering!
	*port = theport; //The port!
	return 1; //Gotten a result!
#endif
	return 0; //Not supported!
}

byte TCP_getremoteV6(sword id, word* ipaddr, word* port)
{
#ifdef GOTNET
	#ifdef SDL3_NET
	word theport;
	#endif
	if (id < 0) return 0; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (!allocatedconnections[id]) return 0; //Not allocated!
	if ((!Client_READY[id]) || (Client_READY[id] > 2)) return 0; //Not connected?
	#ifdef SDL3_NET
	if (remoteaddr[id].protocol==1) //V6 address available?
	{
		memcpy(ipaddr, &remoteaddr[id].V6addr,sizeof(remoteaddr[id].V6addr)); //IP address, unchanged (normal IP address)!
		theport = doSwapBE16(remoteaddr[id].port); //The port, converted to host ordering!
		*port = theport; //The port!
		return 1; //Gotten a result!
	}
	#endif
	return 0; //Gotten no result: V6 protocol not supported!
#endif
	return 0; //Not supported!
}

byte TCP_SendData(sword id, byte data)
{
#ifdef GOTNET
	if (id < 0) return 0; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (!allocatedconnections[id]) return 0; //Not allocated!
	if ((!Client_READY[id]) || (Client_READY[id] > 2)) return 0; //Not connected?
	#ifndef SDL3_NET
	//SDL1/2 NET
	if (SDLNet_TCP_Send(mysock[id], &data, 1) != 1) {
		return 0;
	}
	return 1;
	#else
	if (SDLNet_WaitUntilStreamSocketDrained(mysock[id], 0)) //Drain left?
	{
		return 0; //Not ready to write!
	}
	#if SDL_VERSION_ATLEAST(3, 1, 2)
	if (!SDLNet_WriteToStreamSocket(mysock[id], &data, 1)) //Failed?
	#else
	if (SDLNet_WriteToStreamSocket(mysock[id], &data, 1) != 0) //Failed?
	#endif
	{
		return 0;
	}
	SDLNet_WaitUntilStreamSocketDrained(mysock[id],0); //Wait for it to drain before returning!
	return 1;
	#endif
#endif
	return 0; //Not supported!
}

sbyte TCP_ReceiveData(sword id, byte *result)
{
#ifdef GOTNET
	if (id < 0) return -1; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return -1; //Invalid ID!
	if (!allocatedconnections[id]) return -1; //Not allocated!
	if ((!Client_READY[id]) || (Client_READY[id] > 2)) return -1; //Not connected?
	byte retval = 0;
	#ifndef SDL3_NET
	//SDL1/2 NET
	if(SDLNet_CheckSockets(listensocketset[id],0))
	{
		if(SDLNet_TCP_Recv(mysock[id], &retval, 1)!=1) {
			return -1; //Socket closed
		} else
		{
			*result = retval; //Data read!
			return 1; //Got data!
		}
	}
	else return 0; //No data to receive!
	#else
	if (SDLNet_WaitUntilInputAvailable(&listensocketset[id], 1, 0)) //Anything to parse?
	{
		switch (SDLNet_ReadFromStreamSocket(mysock[id], &retval, 1))
		{
		case 0: //Nothing to read?
			return 0; //No data to receive!
		case 1: //Something read?
			*result = retval; //Data read!
			return 1; //Got data!
			break;
		case -1: //Socket closed?
			return -1;
			break;
		}
	}
	return 0; //Default: no data to receive and nothing to parse!
	#endif
#endif
	return -1; //No socket by default!
}

byte TCP_Connected(sword id)
{
#ifdef GOTNET
	#if defined(SDL2) || defined(SDL3)
	#ifndef SDL3_NET
	const char *error;
	#endif
	#endif
	byte data;
	if (id < 0) return -1; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (!allocatedconnections[id]) return 0; //Not allocated!
	if (!Client_READY[id]) return 0; //Not connected or able to respond?
	#ifndef SDL3_NET
	//SDL1/2 NET
	recheckclientconnection:
	switch (Client_READY[id]) //What is the current status?
	{
	case 1: //Client connection connected
	case 2: //Server connection
		SDLNet_SetError("UNKNOWN!");
		if (SDLNet_CheckSockets(listensocketset[id], 0))
		{
			if (SDLNet_TCP_Recv(mysock[id], &data, 0) != 0)
			{
				return 0; //Socket closed
			}
			else
			{
		#if defined(SDL2) || defined(SDL3)
				error = SDLNet_GetError(); //Any error?
				if (error)
				{
					if (strcmp(error, "UNKNOWN!") != 0) //Not unknown?
					{
						return 0; //Errored out, so disconnected!
					}
				}
		#endif
				return 1; //Got data!
			}
		}
		else
			return 1; //No data to receive!
	case 3: //Client resolving?
		if (threadRunning(client_connectthread[id])) //Still running the resolver?
		{
			return 2; //Busy resolving!
		}
		client_connectthread[id] = NULL; //Finished!

		if (!client_connectthread_result) //Failed to resolve?
		{
			failconnect:
			freeTCPid(id); //Free the used ID!
			return 0; //Failed to connect!
		}

		//Resolved. Start connecting.
		client_connectthread[id] = startThread(&TCP_connectThread, "TCP_connect", &client_connectthread_id[id]); 
		if (client_connectthread[id]) //Success?
		{
			Client_READY[id] = 4; //Next step!
			goto recheckclientconnection;
		}
		else
		{
			Client_READY[id] = 0; //Failed to launch a connect thread!
			goto failconnect; //Fail it!
		}
		break;
	case 4: //Connected or waiting for connection result.
		if (threadRunning(client_connectthread[id])) //Still running the connecting?
		{
			return 2; //Busy resolving!
		}
		client_connectthread[id] = NULL; //Finished!

		//Connection attempt complete. Check the result.
		switch (client_connectthread_result) //What result?
		{
		case 0: //Failed to open the connection
			mysock[id] = NULL; //Deallocated!
			SDLNet_FreeSocketSet(listensocketset[id]); //Free the socket set allocated!
			listensocketset[id] = NULL; //Deallocated!
			freeTCPid(id); //Free the used ID!
			return 0; //Failed to connect!

		case 2: //Failed socket
			//Free resources!
			SDLNet_TCP_Close(mysock[id]); //Close the TCP connection!
			mysock[id] = NULL; //Deallocated!
			SDLNet_FreeSocketSet(listensocketset[id]); //Free the socket set allocated!
			listensocketset[id] = NULL; //Deallocated!
			freeTCPid(id); //Free the used ID!
			return 0; //Failed to connect!
			break;

		case 1: //Connected?
			Client_READY[id] = 1; //Fully connected now!
			goto recheckclientconnection; //Re-check the result!
			break;
		}
	}
	#else
	recheckclientconnection:
	switch (Client_READY[id]) //What ready status?
	{
	case 2: //Server connection active?
	case 1: //Client connection active?
		//SDL3_NET
		if (SDLNet_GetConnectionStatus(mysock[id])) //Connected?
		{
			switch (SDLNet_ReadFromStreamSocket(mysock[id], &data, 0)) //Bluntly check the receiver line without receiving anything!
			{
			case 0: //Nothing to read?
				return 1; //No data to receive!
			case 1: //Something read?
				return 1; //Got data!
				break;
			case -1: //Socket closed?
				return 0; //Not connected!
				break;
			}
		}
		break;
	case 3: //Client resolving?
		//For now, just wait for it to be resolved!
		switch (SDLNet_WaitUntilResolved(client_openip[id], 0)) //Wait for a result!
		{
		case 1: //Resolved?
			Client_READY[id] = 4; //Next step!
			goto recheckclientconnection;
			break;
		case 0: //Still resolving?
			return 2; //Resolving, not connected!
			break;
		case -1: //Failed?
		failedconnect:
			//Failed to resolve?
			freeTCPid(id); //Free the used ID!
			return 0; //Failed to connect!
		}
		break;
	case 4: //Resolved, start connecting.
		//Now it's resolved, try to connect!
		mysock[id] = SDLNet_CreateClient(client_openip[id],
		 client_openport[id]); //Try and connect!
		if (!mysock[id])
		{
			goto failedconnect; //Fail it!
		}
		Client_READY[id] = 5; //Wait for connection
		goto recheckclientconnection;
	case 5: //Connection connecting, wait for connect!
		switch (SDLNet_WaitUntilConnected(mysock[id], 0)) //Try and connect!
		{
		case 1: //Connected?
			listensocketset[id] = mysock[id]; //Same!
			Client_READY[id] = 1; //Connected as a client!
			goto recheckclientconnection; //Successfully connected!
			break;
		case -1: //Failed?
			//Fail it!
			goto failedconnect; //Fail it!
			break;
		case 0: //Still connecting?
			return 2; //Resolving, not connected!
			break;
		}
	}
	return 0; //Default: disconnected!
	#endif
#endif
	return 0; //No socket by default!
}

byte TCP_DisconnectClientServer(sword id)
{
#ifdef GOTNET
	if (id < 0) return 0; //Invalid ID!
	if (id >= NUMITEMS(allocatedconnections)) return 0; //Invalid ID!
	if (!allocatedconnections[id]) return 0; //Not allocated!
	if (!Client_READY[id]) return 0; //Not connected or in a workable state?
	#ifndef SDL3_NET
	if (client_connectthread[id]) //Thread might be running?
	{
		waitThreadEnd(client_connectthread[id]); //Wait for it to finish!
		client_connectthread[id] = NULL;
	}
	//SDL1/2 NET
	if (listensocketset[id])
	{
		if (mysock[id]) //Valid socket to remove?
		{
			SDLNet_TCP_DelSocket(listensocketset[id], mysock[id]); //Remove from the socket set!
		}
		SDLNet_FreeSocketSet(listensocketset[id]); //Free the set!
		listensocketset[id] = NULL; //Not allocated anymore!
	}
	if(mysock[id]) {
		SDLNet_TCP_Close(mysock[id]);
		mysock[id] = NULL; //Not allocated anymore!
	}
	#else
	listensocketset[id] = NULL; //Nothing!
	if (mysock[id]) { //Valid socket to remove?
		SDLNet_DestroyStreamSocket(mysock[id]); //Close the connection!
		mysock[id] = NULL; //Not allocated anymore!
	}
	#endif

	Client_READY[id] = 0; //Ready again!
	if (freeTCPid(id)) //Freed the ID for other uses!
	{
		return 1; //Disconnected!
	}
#endif
	return 0; //Error: not connected!
}

void TCPServer_INTERNAL_stopserver(byte fullstop)
{
#ifdef GOTNET
	if (!NET_READY) return; //Abort when not running properly!
	if (Server_READY==1) //Loaded the server?
	{
		if (fullstop) //Full stop?
		{
			//Disconnect all that's connected!
			#ifndef SDL3_NET
			for (;;)
			{
				//SDL1/2 NET
				TCPsocket new_tcpsock;
				new_tcpsock = SDLNet_TCP_Accept(server_socket);
				if (!new_tcpsock) break; //Stop searching when none!
				SDLNet_TCP_Close(new_tcpsock); //Disconnect the connected client!
			}
			#endif
		}

		#ifndef SDL3_NET
		//SDL1/2 NET
		SDLNet_FreeSocketSet(serverlistensocketset); //Free the socket set allocated!
		SDLNet_TCP_Close(server_socket);
		#else
		SDLNet_DestroyServer(server_socket);
		#endif
		server_socket = NULL; //Nothing anymore!
		serverlistensocketset = NULL; //Nothing anymore!
		--Server_READY; //Layer destroyed!
		if (!fullstop) //Not fully stopped(paused)?
		{
			Server_READY = 2; //We're paused!
		}
	}
#endif
}

void TCPServer_Unavailable()
{
	TCPServer_INTERNAL_stopserver(0); //Perform a partial stop, since we're unavailable!
}

void TCPServer_INTERNAL_startserver()
{
#ifdef GOTNET
	if (Server_READY != 1) //Not started?
	{
		if (freeclientserverconnections() == 0) return; //No available connections prevent restart!
		#ifndef SDL3_NET
		//SDL1/2 NET
		IPaddress ip;
		if (SDLNet_ResolveHost(&ip, NULL, SERVER_PORT) == -1) {
			return; //Failed!
		}

		//Server listen socket set!
		serverlistensocketset = SDLNet_AllocSocketSet(1);
		if (!serverlistensocketset)
		{
			return; //Not ready!
		}

		server_socket = SDLNet_TCP_Open(&ip);
		if (server_socket == NULL) {
			SDLNet_FreeSocketSet(serverlistensocketset); //Free the socket set allocated!
			return; //Failed!
		}

		if (SDLNet_TCP_AddSocket(serverlistensocketset, server_socket) == -1) //Try to listen to it!
		{
			SDLNet_TCP_Close(server_socket); //Close the TCP connection!
			server_socket = NULL; //Nothing!
			SDLNet_FreeSocketSet(serverlistensocketset); //Free the socket set allocated!
			serverlistensocketset = NULL; //Nothing!
		}
		#else
		server_socket = SDLNet_CreateServer(NULL, SERVER_PORT); //Create a server!
		if (server_socket == NULL) {
			serverlistensocketset = NULL; //Nothing!
			return; //Failed!
		}
		serverlistensocketset = server_socket; //Listen socket set!
		#endif

		Server_READY = 1; //First step successful! Ready to receive!
	}
#endif
}

void doneTCP(void) //Finish us!
{
#ifdef GOTNET
	if (NET_READY) //Loaded?
	{
		SDLNet_Quit();
		NET_READY = 0; //Not ready anymore!
	}
#endif

#ifdef IS_PSP
#if defined(GOTNET) || defined(PSP_DISABLEDNET)
	//We require cleanup of the net modules!
	wifi_connected = 0; //Default: not connected to any WiFi!
	int err;
	lock(LOCK_WIFI);
	if (PSP_netmodules_loaded) //Any loaded?
	{
		unlock(LOCK_WIFI);
		disconnectWifi(); //Make sure that WiFi isn't running anymore!
		lock(LOCK_WIFI);
	}
	if (PSP_netmodules_loaded & 2)
	{
		err = sceUtilityUnloadNetModule(PSP_NET_MODULE_INET);
		if (err != 0)
		{
			dolog(NET_LOGFILE, "Error, could not unload PSP_NET_MODULE_INET %08X\n", err);
		}
		PSP_netmodules_loaded &= ~2; //unloaded!
	}
	if (PSP_netmodules_loaded & 1)
	{
		err = sceUtilityUnloadNetModule(PSP_NET_MODULE_COMMON);
		if (err != 0)
		{
			dolog(NET_LOGFILE, "Error, could not unload PSP_NET_MODULE_COMMON %08X\n", err);
		}
		PSP_netmodules_loaded &= ~1; //unloaded!
	}
	unlock(LOCK_WIFI);
#endif
#endif
#ifdef IS_VITA
#if defined(GOTNET)
	lock(LOCK_WIFI);
	if (PSV_netmodules_loaded & 4)
	{
		sceNetCtlTerm();
		PSV_netmodules_loaded &= ~4; //Assume unloaded!
	}

	if (PSV_netmodules_loaded & 2)
	{
		sceNetTerm();
		PSV_netmodules_loaded &= ~2; //Assume unloaded!
	}

	if (PSV_netmodules_loaded & 1)
	{
		sceSysmoduleUnloadModule(SCE_SYSMODULE_NET);
		PSV_netmodules_loaded &= ~1; //Assume unloaded!
	}
	unlock(LOCK_WIFI);
#endif
#endif
#ifdef IS_SWITCH
#if defined(GOTNET)
	lock(LOCK_WIFI);
	if (SWITCH_netmodules_loaded & 1)
	{
		socketExit();
		SWITCH_netmodules_loaded &= ~1;
	}
	unlock(LOCK_WIFI);
#endif
#endif
}

#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
char gottenIP[16] = "";
byte GOTNETCONNECT = 1;
byte WiFiThreadState = 0; //State of the wifi thread!
byte WiFiThreadRequest = 0; //Requesting anything of the WiFi thread?
ThreadParams_p WiFiThread = NULL; //Internet thread, if any!
GPU_TEXTSURFACE* TCP_Surface; //Our very own BIOS Surface!
void allocTCPsurface()
{
	TCP_Surface = alloc_GPUtext(); //Allocate a BIOS Surface!
	if (!TCP_Surface)
	{
		raiseError("BIOS", "Ran out of memory allocating BIOS Screen Layer!");
		return; //Just in case!
	}
	GPU_addTextSurface(TCP_Surface, NULL); //Register our text surface!
}
void freeTCPsurface()
{
	lock(LOCK_WIFI); //make sure Wifi isn't using it!
	GPU_removeTextSurface(TCP_Surface); //Unregister!
	free_GPUtext(&TCP_Surface); //Try to deallocate the BIOS Menu surface!
	unlock(LOCK_WIFI);
}

//Cancelled!
#define FILELIST_CANCEL -1
//No files with this extension!
#define FILELIST_NOFILES -2
//Default item!
#define FILELIST_DEFAULT -3

//Amount of files in the list MAX
#define ITEMLIST_MAXITEMS 128
char TCP_itemlist[ITEMLIST_MAXITEMS][256]; //Max X files listed!
char TCP_itemlistdetail[ITEMLIST_MAXITEMS][256]; //Max X files listed!
int TCP_itemlistindex[ITEMLIST_MAXITEMS]; //Index used!
word TCP_numlist = 0; //Number of files!

byte TCP_itemlist_x = 0;
byte TCP_itemlist_y = 2; //Never lower than this!
byte TCP_itemlist_inuse = 0; //Flags in-use, meaning that TCP_itemlist_x&y are not to be changed by the user!

void TCP_clearList()
{
	memset(&TCP_itemlist, 0, sizeof(TCP_itemlist)); //Init!
	TCP_numlist = 0; //Nothin in there yet!
}

void TCP_addList(char* name, char *detail, int iNetIndex)
{
	if (TCP_numlist < ITEMLIST_MAXITEMS) //Maximum not reached yet?
	{
		safestrcpy(TCP_itemlist[TCP_numlist], sizeof(TCP_itemlist[0]), name); //Add the item!
		safestrcpy(TCP_itemlistdetail[TCP_numlist], sizeof(TCP_itemlistdetail[0]), detail); //Add the item!
		TCP_itemlistindex[TCP_numlist] = iNetIndex; //Add the item and increase!
		++TCP_numlist; //One processed!
	}
}

void TCP_printCurrent(int x, int y, int result, int maxlen) //Create the current item with maximum length!
{
	char* text;
	char buffer[1024]; //The buffered text!
	char filler[1024]; //The filler data!
	char buffer2[1024]; //The buffered text!
	char filler2[1024]; //The filler data!
	text = TCP_itemlist[result]; //Retrieve the item to print from the list!
	memset(&buffer, '\0', sizeof(buffer)); //Init buffer to unused!
	memset(&filler, '\0', sizeof(filler)); //Init filler to unused!
	int i, j;
	int max, max2;
	max = safe_strlen(text, 256); //Default: maximum the size of the destination!
	if (max > maxlen) //More than maximum length?
	{
		max = maxlen; //Truncate to maximum length!
	}
	if (max > (int)(sizeof(buffer) - 1)) //Too much?
	{
		max = sizeof(buffer) - 1; //Limit to buffer!
	}
	//First: compose text with limit!
	for (i = 0; i < max;) //Process the entire length, up to maximum processable length!
	{
		buffer[i] = text[i]; //Set as text!
		++i; //Next item!
	}
	//Next: compose filler!
	j = 0; //Init second filler!
	max2 = maxlen; //Maximum length!
	if (max2 > (int)(sizeof(filler) - 1)) //Limit breached?
	{
		max2 = sizeof(filler) - 1; //Limit!
	}
	for (; i < max2; i++) //Anything left?
	{
		filler[j++] = ' '; //Fill filler!
	}

	text = TCP_itemlistdetail[result]; //Retrieve the item to print from the list!
	memset(&buffer2, '\0', sizeof(buffer2)); //Init buffer to unused!
	memset(&filler2, '\0', sizeof(filler2)); //Init filler to unused!
	max = safe_strlen(text, 256); //Default: maximum the size of the destination!
	if (max > maxlen) //More than maximum length?
	{
		max = maxlen; //Truncate to maximum length!
	}
	if (max > (int)(sizeof(buffer) - 1)) //Too much?
	{
		max = sizeof(buffer) - 1; //Limit to buffer!
	}
	//First: compose text with limit!
	for (i = 0; i < max;) //Process the entire length, up to maximum processable length!
	{
		buffer2[i] = text[i]; //Set as text!
		++i; //Next item!
	}
	//Next: compose filler!
	j = 0; //Init second filler!
	max2 = maxlen; //Maximum length!
	if (max2 > (int)(sizeof(filler) - 1)) //Limit breached?
	{
		max2 = sizeof(filler) - 1; //Limit!
	}
	for (; i < max2; i++) //Anything left?
	{
		filler2[j++] = ' '; //Fill filler!
	}


	//Finally, print the output!
	GPU_text_locksurface(TCP_Surface);
	GPU_textgotoxy(TCP_Surface, x, y);
	GPU_textprintf(TCP_Surface,RGB(0xFF,0xFF,0xFF),RGB(0x00,0x00,0x00), "%s", buffer); //Show item with maximum length or less!
	GPU_textprintf(TCP_Surface,RGB(0xFF,0xFF,0xFF),RGB(0x00,0x00,0x00), "%s", filler); //Show rest with filler color, update!
	GPU_textgotoxy(TCP_Surface, x, (y+1));
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "%s", buffer2); //Show item with maximum length or less!
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "%s", filler2); //Show rest with filler color, update!
	GPU_text_releasesurface(TCP_Surface);
}

//x,y = coordinates of file list
//maxlen = amount of characters for the list (width of the list)

int TCP_ExecuteList(int x, int y, int maxlen, int blockActions) //Runs the file list!
{
	TCP_itemlist_inuse = 1; //Force used, if not already!
	unlock(LOCK_WIFI); //Item list in use?
	char currentstart;
	int resultcopy;
	int key = 0;
	//First, no file check!
	if (!TCP_numlist) //No files?
	{
		GPU_text_locksurface(TCP_Surface);
		GPU_textgotoxy(TCP_Surface, x,y);
		GPU_textprintf(TCP_Surface,RGB(0xFF,0xFF,0xFF),RGB(0x00,0x00,0x00), "No network connections found!");
		GPU_text_releasesurface(TCP_Surface);
		lock(LOCK_WIFI);
		TCP_itemlist_inuse = 0; //Not used anymore!
		unlock(LOCK_WIFI);
		return FILELIST_NOFILES; //Error: no files found!
	}

	//Now, find the default!
	int result = 0; //Result!
	result = 0; //First entry to start with always!

	TCP_printCurrent(x, y, result, maxlen); //Create our current entry!

	while (1) //Doing selection?
	{
		if (shuttingdown()) //Cancel?
		{
			lock(LOCK_WIFI);
			TCP_itemlist_inuse = 0; //Not used anymore!
			unlock(LOCK_WIFI);
			return FILELIST_CANCEL; //Cancel!
		}
		lock(LOCK_INPUT);
		key = psp_inputkeydelay(TCPMENU_INPUTDELAY); //Input key!
		unlock(LOCK_INPUT);

		if ((key & BUTTON_UP) > 0) //UP?
		{
			if (result > 0) //Not first?
			{
				--result; //Up one item!
			}
			else //First?
			{
				result = TCP_numlist - 1; //Bottom of the list!
			}
			TCP_printCurrent(x, y, result, maxlen); //Create our current entry!
		}
		else if ((key & BUTTON_DOWN) > 0) //DOWN?
		{
			if (result < (TCP_numlist - 1)) //Not at the bottom?
			{
				++result; //Down one item!
			}
			else //At the bottom?
			{
				result = 0; //Top of the list!
			}
			TCP_printCurrent(x, y, result, maxlen); //Create our current entry!
		}
		else if ((key & BUTTON_LEFT) > 0) //LEFT?
		{
			currentstart = TCP_itemlist[result][0]; //The character to check against!
			resultcopy = result; //What item to go to!
			for (; ((resultcopy > 0) && (currentstart == TCP_itemlist[resultcopy][0])); --resultcopy); //While still the same? Scroll up until we don't or reach the first item!
			if (TCP_itemlist[resultcopy][0] != currentstart) //Valid result?
			{
				result = resultcopy; //Go to the item that's found!
			}
			TCP_printCurrent(x, y, result, maxlen); //Create our current entry!
		}
		else if ((key & BUTTON_RIGHT) > 0) //RIGHT?
		{
			currentstart = TCP_itemlist[result][0]; //The character to check against!
			resultcopy = result; //What item to go to!
			for (; ((resultcopy < (TCP_numlist - 1)) && (currentstart == TCP_itemlist[resultcopy][0])); ++resultcopy); //While still the same? Scroll up until we don't or reach the first item!
			if (TCP_itemlist[resultcopy][0] != currentstart) //Valid result?
			{
				result = resultcopy; //Go to the item that's found!
			}
			TCP_printCurrent(x, y, result, maxlen); //Create our current entry!
		}
		else if (((key & (~blockActions) & (BUTTON_CONFIRM | BUTTON_START)) > 0) /* || (key & BUTTON_PLAY && BIOS_EnablePlay)*/) //OK?
		{
			delay(500000); //Wait a bit before continuing!
			lock(LOCK_WIFI);
			TCP_itemlist_inuse = 0; //Not used anymore!
			unlock(LOCK_WIFI);
			return result; //Give the result!
		}
		else if ((key & BUTTON_CANCEL) > 0) //CANCEL?
		{
			lock(LOCK_WIFI);
			TCP_itemlist_inuse = 0; //Not used anymore!
			unlock(LOCK_WIFI);
			return FILELIST_CANCEL; //Cancelled!
		}
		else if ((key & (~blockActions) & BUTTON_TRIANGLE) > 0) //DEFAULT?
		{
			lock(LOCK_WIFI);
			TCP_itemlist_inuse = 0; //Not used anymore!
			unlock(LOCK_WIFI);
			return FILELIST_DEFAULT; //Unmount!
		}
	}
}

void inetThread()
{
	int last_state;
	u32 err;
	union SceNetApctlInfo info;

	int connectionConfig = -1;
	int iNetIndex;
	int iPick;

	lock(LOCK_WIFI);
	safestrcpy(gottenIP, sizeof(gottenIP), ""); //Empty!
	wifi_connected = 0; //Not connected!
	WiFiThreadState = 0xFF; //Initializing!
	unlock(LOCK_WIFI);

	err = pspSdkInetInit();

	if (err != 0) {
		GPU_text_locksurface(TCP_Surface);
		GPU_textgotoxy(TCP_Surface, 0, 0);
		GPU_textprintf(TCP_Surface,RGB(0xFF,0xFF,0xFF),RGB(0x00,0x00,0x00),"pspSdkInetInit: %d !\n", err);
		GPU_text_releasesurface(TCP_Surface);
		delay(5000000); //Wait a bit!
		lock(LOCK_WIFI);
		safestrcpy(gottenIP, sizeof(gottenIP), ""); //Empty!
		wifi_connected = 0; //Not connected!
		WiFiThreadState = 0; //Finishing!
		unlock(LOCK_WIFI);
		GPU_text_locksurface(TCP_Surface);
		GPU_textclearscreen(TCP_Surface); //Clear the screen now!
		GPU_text_releasesurface(TCP_Surface);
		return;
	}

	while (sceWlanGetSwitchState() != 1)  //switch = off
	{
		GPU_text_locksurface(TCP_Surface);
		GPU_textgotoxy(TCP_Surface, 0, 0);
		GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Turn the wifi switch on!");
		GPU_text_releasesurface(TCP_Surface);
		lock(LOCK_INPUT);
		if (psp_inputkeydelay(1000) & (BUTTON_CIRCLE))
		{
			for (; psp_inputkeydelay(1000) & (BUTTON_CIRCLE);) //While pressed...
			{
				unlock(LOCK_INPUT);
				delay(1000); //Wait a bit!
				lock(LOCK_INPUT);
			}
			unlock(LOCK_INPUT);
			goto close_net;
		}
		unlock(LOCK_INPUT);
	}

	lock(LOCK_WIFI);
	WiFiThreadState = 2; //Preparing for inputting...
	unlock(LOCK_WIFI);

	// enumerate connections
	GPU_text_locksurface(TCP_Surface);
	GPU_textgotoxy(TCP_Surface, 0, 0);
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Retrieving connection list...");
	GPU_text_releasesurface(TCP_Surface);

	for (iNetIndex = 1; iNetIndex < 100; iNetIndex++) // skip the 0th connection
	{
		netData data;
		netData name;
		char detail[256];
		if (sceUtilityCheckNetParam(iNetIndex) != 0) continue;
		sceUtilityGetNetParam(iNetIndex, 0, &name);

		sceUtilityGetNetParam(iNetIndex, 1, &data);
		safe_strcpy(detail,sizeof(detail),"SSID=");
		safe_strcat(detail,sizeof(detail),data.asString);

		sceUtilityGetNetParam(iNetIndex, 4, &data);
		if (data.asString[0]) {
			// not DHCP
			sceUtilityGetNetParam(iNetIndex, 5, &data);
			safe_strcat(detail,sizeof(detail), " IPADDR=");
			safe_strcat(detail,sizeof(detail), data.asString);
		}
		else {
			safe_strcat(detail,sizeof(detail), " DHCP");
		}

		TCP_addList(name.asString, detail, iNetIndex);
		if (TCP_numlist >= ITEMLIST_MAXITEMS) break;  // no more
	}

	if (TCP_numlist == 0) {
		//pgFramePrint(0, 0, "No wifi connections, please create one !", PG_TEXT_COLOR);
		goto close_net;
	}

	GPU_text_locksurface(TCP_Surface);
	GPU_textgotoxy(TCP_Surface, 0, 0);
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Choose a connection: Press " STR_UTF8_CIRCLE " to cancel");
	GPU_text_releasesurface(TCP_Surface);

	lock(LOCK_WIFI);
	WiFiThreadState = 3; //Inputting connection...
	TCP_itemlist_inuse = 1; //The list is now in use!
	if (TCP_itemlist_y < 3) //Reserved rows?
	{
		TCP_itemlist_y = 3; //Force down first!
	}
	GPU_text_locksurface(TCP_Surface);
	GPU_textgotoxy(TCP_Surface, TCP_itemlist_x, TCP_itemlist_y);
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Access point: ");
	GPU_text_releasesurface(TCP_Surface);
	iPick = TCP_ExecuteList(TCP_itemlist_x+14,TCP_itemlist_y,((80-TCP_itemlist_x)-14),0); //Execute the list! Take the starting location and text display for the selection into account! This requires to WiFi be locked!
	if (iPick<0) goto close_net;
	connectionConfig = TCP_itemlistindex[iPick]; //Get the used configuration!

	//Cleanup the dialog first!
	GPU_text_locksurface(TCP_Surface);
	GPU_textclearscreen(TCP_Surface); //Clear the screen now!
	GPU_text_releasesurface(TCP_Surface);

	//Only the cancel option should remain!
	GPU_text_locksurface(TCP_Surface);
	GPU_textgotoxy(TCP_Surface, 0, 4);
	GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Press " STR_UTF8_CIRCLE " to cancel");
	GPU_text_releasesurface(TCP_Surface);

connect_wifi:

	lock(LOCK_WIFI);
	WiFiThreadState = 4; //Connecting to a WiFi network!
	wifi_connected = 0; //Ww're not connected!
	safestrcpy(gottenIP, sizeof(gottenIP), ""); //Empty!
	unlock(LOCK_WIFI);

	err = sceNetApctlConnect(connectionConfig);
	if (err != 0) {
		goto close_net;
	}

	{
		char buffer[128];
		strcpy(buffer, "Trying to connect to ");
		strcat(buffer, TCP_itemlist[iPick]);
		GPU_text_locksurface(TCP_Surface);
		GPU_textgotoxy(TCP_Surface, 0, 0);
		GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "%s", buffer);
		GPU_text_releasesurface(TCP_Surface);
	}

	// 4 connected with IP address

	lock(LOCK_WIFI);
	WiFiThreadState = 5; //Connecting to a WiFi network, waiting for completion!
	wifi_connected = 0; //Ww're not connected!
	safestrcpy(gottenIP, sizeof(gottenIP), ""); //Empty!
	unlock(LOCK_WIFI);

	last_state = -1;

	int state = 0;
	while (1)
	{
		char buffer[128];

		lock(LOCK_INPUT);
		if (psp_inputkeydelay(1000) & (BUTTON_CIRCLE))
		{
			for (; psp_inputkeydelay(50000) & (BUTTON_CIRCLE);) //While pressed...
			{
				unlock(LOCK_INPUT);
				delay(1000); //Wait a bit!
				lock(LOCK_INPUT);
			}
			unlock(LOCK_INPUT);
			goto close_connection;
		}
		unlock(LOCK_INPUT);

		err = sceNetApctlGetState(&state);

		if (err != 0)
		{
			GPU_text_locksurface(TCP_Surface);
			GPU_textgotoxy(TCP_Surface, 0, 2);
			GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Connection failed !");
			GPU_text_releasesurface(TCP_Surface);
			goto close_connection;
		}

		if (state != last_state)
		{
			if (last_state == 2 && state == 0)
			{
				GPU_text_locksurface(TCP_Surface);
				GPU_textgotoxy(TCP_Surface, 0, 2);
				GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "Connecting to wifi Failed, Retrying...");
				GPU_text_releasesurface(TCP_Surface);
				sceKernelDelayThread(500 * 1000); // 500ms
				goto connect_wifi;
			}

			snprintf(buffer,sizeof(buffer), "Connection state %d of 4", state);
			GPU_text_locksurface(TCP_Surface);
			GPU_textgotoxy(TCP_Surface, 0, 1);
			GPU_textprintf(TCP_Surface, RGB(0xFF, 0xFF, 0xFF), RGB(0x00, 0x00, 0x00), "%s", buffer);
			GPU_text_releasesurface(TCP_Surface);
			last_state = state;
		}

		// 0 - idle
		// 1,2 - starting up
		// 3 - waiting for dynamic IP
		// 4 - got IP - usable
		if (state == 4) break;  // connected with static IP
	}

	// get IP address
	if (sceNetApctlGetInfo(8, &info) != 0)
	{
		safestrcpy(info.ip, sizeof(info.ip), ""); //Unknown!
	}


	GPU_text_locksurface(TCP_Surface);
	GPU_textclearscreen(TCP_Surface); //Clear the screen now!
	GPU_text_releasesurface(TCP_Surface);

	lock(LOCK_WIFI);
	WiFiThreadState = 1; //Connected to any WiFi network!
	wifi_connected = 1; //Ww're connected!
	snprintf(gottenIP, sizeof(gottenIP), "%s", info.ip); //The gotten IP, if known!
	unlock(LOCK_WIFI);

	lock(LOCK_WIFI);
	for (;((WiFiThreadRequest != 1) && (sceWlanGetSwitchState() == 1));) //Not requesting disconnect or turned off in hardware?
	{
		unlock(LOCK_WIFI);
		delay(50*1000); //Some time to pass!
		lock(LOCK_WIFI);

		err = sceNetApctlGetState(&state);

		if (err != 0)
		{
			unlock(LOCK_WIFI);
			goto close_connection;
		}

		if (state != last_state)
		{
			if (((last_state == 2) || (last_state == 4)) && (state == 0))
			{
				unlock(LOCK_WIFI);
				goto close_connection;
			}

			last_state = state;
		}

		// 0 - idle
		// 1,2 - starting up
		// 3 - waiting for dynamic IP
		// 4 - got IP - usable
		if (state != 4) break;  // keep being here while connected with static IP
	}
	unlock(LOCK_WIFI);
	//We're requested to disconnect!

close_connection:
	err = sceNetApctlDisconnect();

close_net:
	//Closing the connection!
	pspSdkInetTerm();
	GPU_text_locksurface(TCP_Surface);
	GPU_textclearscreen(TCP_Surface); //Clear the screen now!
	GPU_text_releasesurface(TCP_Surface);
	sceKernelDelayThread(1000000);
	lock(LOCK_WIFI);
	wifi_connected = 0; //Not connected anymore!
	WiFiThreadState = 0; //Not running anymore!
	wifi_connected = 0; //Ww're not connected!
	safestrcpy(gottenIP, sizeof(gottenIP), ""); //Empty!
	unlock(LOCK_WIFI);
}
#else
byte GOTNETCONNECT = 0; //No connection support!
#endif

void connectWifi()
{
#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
	lock(LOCK_WIFI);
	if (wifi_connected) //Already connected?
	{
		unlock(LOCK_WIFI);
		return; //Abort!
	}
	WiFiThreadRequest = 0; //Nothing requested, otherwise requesting to connect!
	//Not connected, then what?
	if (WiFiThreadState > 1) //Connecting?
	{
		WaitForWifiThreadCompletion: //Waiting for it to finish?
		for (; (WiFiThreadState > 1);) //Connection busy?
		{
			unlock(LOCK_WIFI);
			delay(0); //Wait for connection!
			lock(LOCK_WIFI);
		}
		unlock(LOCK_WIFI); //Finish up!
		return; //Finshed!
	}
	if (!WiFiThreadState) //Possible to start?
	{
		unlock(LOCK_WIFI);
		lock(LOCK_THREADHOLDERS);
		WiFiThread = startThread(&inetThread, "UniPCemu_BIOSMenu", NULL); //Start the BIOS menu thread!
		unlock(LOCK_THREADHOLDERS);
		lock(LOCK_WIFI); //We're to start waiting on it!
	}
	goto WaitForWifiThreadCompletion;
#endif
}

void disconnectWifi()
{
#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
	lock(LOCK_WIFI);
	if (!WiFiThread)
	{
		unlock(LOCK_WIFI);
		return; //Nothing to terminate!
	}
	WiFiThreadRequest = 1; //Request termination!
	for (; (WiFiThreadState >= 1);) //Connection busy?
	{
		unlock(LOCK_WIFI);
		delay(0); //Wait for connection!
		lock(LOCK_WIFI);
	}
	WiFiThread = NULL; //No thread anymore: it's cleaned up!
	unlock(LOCK_WIFI); //Finish up!
#endif
}

byte wifiConnected()
{
#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
	byte result;
	lock(LOCK_WIFI);
	result = wifi_connected; //Give the wifi connected status!
	unlock(LOCK_WIFI);
	return result; //Give the result!
#else
	return (wifi_connected==1); //Unsupported: assume always connected!
#endif
}

byte wifiGetIP(char* ip, size_t size)
{
#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
	byte result;
	lock(LOCK_WIFI);
	result = wifi_connected; //Give the wifi connected status!
	if (result) //Connected? Required to have an IP!
	{
		if (!gottenIP[0]) //Not gotten an IP?
		{
			result = 0; //Fail!
		}
		else
		{
			safestrcpy(ip, size, gottenIP); //Get the IP that's gotten!
		}
	}
	unlock(LOCK_WIFI);
	return result; //Give the result!
#else
	return 0; //Unsupported never gotten a result!
#endif
}

/*
WifiSetAccessPointLocation: Sets the location for the access point inputs.
parameters:
	x: The x location
	y: The y location
result:
	0: Not changed (in use), 1: Changed, 2: Unsupported action.
*/

byte wifiSetAccessPointLocation(int x, int y)
{
#if defined(IS_PSP) && (defined(GOTNET) || defined(PSP_DISABLEDNET))
	lock(LOCK_WIFI);
	if (TCP_itemlist_inuse) //In use?
	{
		unlock(LOCK_WIFI);
		return 0; //Can't change when in-use!
	}
	TCP_itemlist_x = x; //Where to ...
	TCP_itemlist_y = y; //Display the list!
	unlock(LOCK_WIFI);
	return 1; //Give the result: success!
#else
	return 2; //Unsupported never gotten a result!
#endif
}