/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

//Biquad filter (https://searchfox.org/mozilla-central/source/dom/media/webaudio/blink/Biquad.cpp) for type 2/3 filters
/*
 * Copyright (C) 2010 Google Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1.  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2.  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 * 3.  Neither the name of Apple Computer, Inc. ("Apple") nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE AND ITS CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL APPLE OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "headers/support/filters.h" //Our filter definitions!

void applySoundBiquadFilter(void *filter, float *currentsample)
{
	HIGHLOWPASSFILTER *f = (HIGHLOWPASSFILTER *)filter; //Filter!
	float in = *currentsample; //Read input sample!
	double out = f->biquad.b0 * in + f->biquad.b1 * f->biquad.x1 + f->biquad.b2 * f->biquad.x2 - f->biquad.a1 * f->biquad.y1 - f->biquad.a2 * f->biquad.y2;
	f->biquad.x2 = f->biquad.x1;
	f->biquad.x1 = in;
	f->biquad.y2 = f->biquad.y1;
	f->biquad.y1 = out;
	if (f->biquad.x1==0.0 &&f->biquad.x2==0.0 && (f->biquad.y1!=0.0 ||f->biquad.y2!=0.0) && (fabs(f->biquad.y1)<FLT_MIN) && (fabs(f->biquad.y2)<FLT_MIN)) //Safety
	{
		f->biquad.y1 = f->biquad.y2 = 0.0;
		out = 0.0; //No denormal
	}
    *currentsample = (float)out; //Write output sample!
}

void soundfilter_setNormalizedCoefficients(HIGHLOWPASSFILTER *filter, double b0, double b1, double b2, double a0, double a1, double a2)
{
	double a0inverse = 1.0/a0;
	filter->biquad.b0 = b0*a0inverse;
	filter->biquad.b1 = b1*a0inverse;
	filter->biquad.b2 = b2*a0inverse;
	filter->biquad.a1 = a1*a0inverse;
	filter->biquad.a2 = a2*a0inverse;
}

void updateSoundFilter(HIGHLOWPASSFILTER *filter, byte ishighpass, float cutoff_freq, float q, double gainreduction, float samplerate)
{
	ishighpass &= 3; //Filter valid values!
	if (filter->isInit || (filter->cutoff_freq!=cutoff_freq) || (filter->q!=q) || (filter->gainreduction!=gainreduction) || (filter->samplerate!=samplerate) || (ishighpass!=filter->isHighPass)) //We're to update?
	{
		if (unlikely((ishighpass!=filter->isHighPass) && (filter->isInit==0))) return; //Don't allow changing filter types of running channels!
		if (ishighpass<2) //Original RC filter?
		{
			if (unlikely((filter->samplerate != samplerate) || filter->isInit)) //New sample rate?
			{
				filter->dt = (1.0f / samplerate); //New delta-time pre-calc!
			}
			if (likely((filter->cutoff_freq != cutoff_freq) || filter->isInit)) //New cutoff freq?
			{
				filter->rc = (1.0f / (cutoff_freq * filter->doublePI)); //New RC pre-calc!
			}
			if (ishighpass) //High-pass filter?
			{
				float RC = filter->rc; //RC is used multiple times, calculate once!
				filter->alpha = (RC / (RC + filter->dt)); //Alpha value to use!
			}
			else //Low-pass filter?
			{
				float dt = filter->dt; //DT is used multiple times, calculate once!
				filter->alpha = (dt / (filter->rc + dt)); //Alpha value to use!
			}
		}
		//Indicate our updated precalcs!
		filter->isHighPass = ishighpass; //Hi-pass filter? Reserve values 2/3 for 0/1 alternatives (with Q resonance support)!
		filter->cutoff_freq = cutoff_freq; //New cutoff frequency!
		filter->samplerate = samplerate; //New samplerate!
		filter->q = q; //Q resonance!
		filter->gainreduction = gainreduction; //Gain reduction (1.0 is  none)
		//filter->peakGain = 0.0f; //Peak gain.
		if (filter->isHighPass&2) //Biquad filter?
		{
			double a0,a1,a2,b0,b1,b2;
			double g=(double)q; //10^(0.05*resonance)
			//V = powf(10.0f,fabs(filter->peakGain) / 20.0f); //Gain
			filter->biquad.Fc = (cutoff_freq/filter->samplerate); //Effective filter cutoff! Fc=F0/Fs
			filter->biquad.Fc = LIMITRANGE(filter->biquad.Fc,0.0,1.0); //Safety
			double w0=PI*filter->biquad.Fc;
			double cos_w0=cos(w0);
			double alpha=sin(w0)/(2.0*g);
			if (filter->isHighPass==2) //Low-pass filter?
			{
				if (filter->biquad.Fc==1.0)
				{
					soundfilter_setNormalizedCoefficients(filter,1.0,0.0,0.0,1.0,0.0,0.0); //Z-transform 1
				}
				else if (filter->biquad.Fc>0.0)
				{
					b1 = 1.0 - cos_w0;
					b0 = (0.5 * b1)*gainreduction;
					b1 *= gainreduction;
					b2 = b0;
					a0 = 1.0 + alpha;
					a1 = -2.0 * cos_w0;
					a2 = 1.0 - alpha;
					soundfilter_setNormalizedCoefficients(filter,b0,b1,b2,a0,a1,a2); //Z-transform 1
				}
				else //Zero?
				{
					soundfilter_setNormalizedCoefficients(filter,0.0,0.0,0.0,1.0,0.0,0.0); //Z-transform 1
				}
			}
			else //High-pass filter?
			{
				if (filter->biquad.Fc==1.0)
				{
					soundfilter_setNormalizedCoefficients(filter,0.0,0.0,0.0,1.0,0.0,0.0); //Z-transform 0
				}
				else if (filter->biquad.Fc>0.0)
				{
					b1 = -1.0 - cos_w0;
					b0 = (-0.5 * b1)*gainreduction;
					b1 *= gainreduction;
					b2 = b0;
					a0 = 1.0 + alpha;
					a1 = -2.0 * cos_w0;
					a2 = 1.0 - alpha;
					soundfilter_setNormalizedCoefficients(filter,b0,b1,b2,a0,a1,a2); //Z-transform 1
				}
				else //Zero?
				{
					soundfilter_setNormalizedCoefficients(filter,1.0,0.0,0.0,1.0,0.0,0.0); //Z-transform 1
				}
			}
			filter->slowhandler = &applySoundBiquadFilter; //Apply the slow handler for parsing the samples!
		}
		else //Normal handler!
		{
			filter->slowhandler = NULL; //Default: no custom handler!
		}
	}
}

void initSoundFilter(HIGHLOWPASSFILTER *filter, byte ishighpass, float cutoff_freq, float q, double gainreduction, float samplerate)
{
	filter->isInit = 1; //We're an Init!
	filter->sound_last_result = filter->sound_last_sample = 0; //Save the first&last sample!
	filter->doublePI = (2.0f * (float)PI); //Twice PI!
	filter->biquad.x1 = filter->biquad.x2 = filter->biquad.y1 = filter->biquad.y2 = 0.0; //Default!
	updateSoundFilter(filter,ishighpass,cutoff_freq,q,gainreduction,samplerate); //Init our filter!
	filter->isInit = 0; //No init anymore: don't update changed values anymore!
}

void applySoundFilter(HIGHLOWPASSFILTER *filter, float *currentsample)
{
	if (filter->slowhandler) //Slow handler?
	{
		applySoundBiquadFilter(filter,currentsample); //Slow handler is Biquad!
	}
	else //Fast handler?
	{
		INLINEREGISTER float last_result;
		last_result = filter->sound_last_result; //Load the last result to process!
		if (unlikely(filter->isHighPass)) //High-pass filter? Low-pass filters are more commonly used!
		{
			last_result = filter->alpha * (last_result + *currentsample - filter->sound_last_sample);
			filter->sound_last_sample = *currentsample; //The last sample that was processed!
		}
		else //Low-pass filter?
		{
			last_result += (filter->alpha*(*currentsample-last_result));
		}
		*currentsample = filter->sound_last_result = last_result; //Give the new result!
	}
}

void applySoundHighPassFilter(HIGHLOWPASSFILTER *filter, float *currentsample)
{
	if (filter->slowhandler) //Slow handler?
	{
		applySoundBiquadFilter(filter,currentsample); //Slow handler is Biquad!
	}
	else //Fast handler?
	{
		INLINEREGISTER float last_result;
		last_result = filter->sound_last_result; //Load the last result to process!
		last_result = filter->alpha * (last_result + *currentsample - filter->sound_last_sample);
		filter->sound_last_sample = *currentsample; //The last sample that was processed!
		*currentsample = filter->sound_last_result = last_result; //Give the new result!
	}
}

#define calcSoundLowPassFilter(filter,currentsample) *currentsample = filter->sound_last_result = filter->sound_last_result+(filter->alpha*((*currentsample)-filter->sound_last_result))

void applySoundLowPassFilter(HIGHLOWPASSFILTER *filter, float *currentsample)
{
	if (filter->slowhandler) //Biquad?
	{
		applySoundBiquadFilter(filter,currentsample); //Slow handler is Biquad!
	}
	else //Fast handler?
	{
		calcSoundLowPassFilter(filter,currentsample); //Filter manually!
	}
}
