LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

APP_OPTIM := release

LOCAL_MODULE := main

#Default: build using SDL2 and compatibles.
BUILD_SDL := SDL2
ifneq (,$(ANDROIDSTUDIO))
ifneq (,$(findstring SDL3,$(INSTALLED_MODULES_MAKEFILES)))
# Building using SDL3 and compatibles instead of SDL2
BUILD_SDL := SDL3
endif
endif

SDL_PATH := ../../android-project/jni/$(BUILD_SDL)
SDL_net_PATH := ../../android-project/jni/$(BUILD_SDL)_net

ROOTPATH = $(LOCAL_PATH)/../../$(PROJECT_DIRNAME)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(ROOTPATH) $(ROOTPATH)/../commonemuframework

PLATFORM = custom

include $(LOCAL_PATH)/../../$(PROJECT_DIRNAME)/Makefile

OBJS := $(PROJECT_OBJS)

SDLMAINC :=
ifneq ($(BUILD_SDL),SDL3)
#Using SDL_android_main.c
SDLMAINC := $(SDL_PATH)/src/main/android/SDL_android_main.c
endif

# Add your application source files here...
LOCAL_SRC_FILES := $(SDLMAINC) \
	$(patsubst %.o,../../../$(PROJECT_DIRNAME)/%.c,$(OBJS))

LOCAL_STATIC_LIBRARIES := $(BUILD_SDL)_static

LOCAL_CFLAGS := -D$(BUILD_SDL) -D$(BUILD_DEFINE) -Wall -std=gnu99

ifneq (,$(ANDROIDSTUDIO))
ifneq (,$(findstring $(BUILD_SDL)_net,$(INSTALLED_MODULES_MAKEFILES)))
LOCAL_CFLAGS := $(LOCAL_CFLAGS) -D$(BUILD_SDL)_NET -DNOPCAP -DPACKETSERVER_ENABLED
LOCAL_STATIC_LIBRARIES := $(LOCAL_STATIC_LIBRARIES) $(BUILD_SDL)_net
LOCAL_C_INCLUDES := $(LOCAL_C_INCLUDES) $(LOCAL_PATH)/$(SDL_net_PATH)
endif
endif

ifeq (1,$(use$(BUILD_SDL)_net))
LOCAL_CFLAGS := $(LOCAL_CFLAGS) -D$(BUILD_SDL)_NET -DNOPCAP -DPACKETSERVER_ENABLED
LOCAL_STATIC_LIBRARIES := $(LOCAL_STATIC_LIBRARIES) $(BUILD_SDL)_net
LOCAL_C_INCLUDES := $(LOCAL_C_INCLUDES) $(LOCAL_PATH)/$(SDL_net_PATH)
endif

ifeq (1,$(ANDROIDSTUDIO))
LOCAL_CFLAGS := $(LOCAL_CFLAGS) -D__DISABLE_INLINE
endif

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog


#Profiler support block
ifneq (,$(findstring NDK_PROFILE,$(MAKECMDGOALS)))
IS_PROFILE = 1
endif

ifneq (,$(findstring NDK_LINEPROFILE,$(MAKECMDGOALS)))
#Profiling detected
IS_PROFILE = 1
endif

ifeq (,$(NDK_PROFILE))
#Make us equal for easy double support!
NDK_PROFILE = $(NDK_LINEPROFILE)
endif

#Check for empty profile target!
ifeq ($(IS_PROFILE),1)
ifeq ($(NDK_PROFILE),)
$(error Please specify the directory containing the android-ndk-profiler directory, which contains the profiler's Android.mk file, by specifying "NDK_PROFILE=YOURPATHHERE"(without quotes). Replace YOURPATHHERE with the ndk profiler Android.mk directory path. This is usually the sources path(where android-ndk-profiler/Android.mk is located after relocation of the jni folder content). Use NDK_LINEPROFILE instead to specify line-by-line profiling. )
endif
endif

#Apply profile support!
PROFILE_CFLAGS = -pg
ifneq (,$(NDK_LINEPROFILE))
#Enable line profiling!
PROFILE_CFLAGS := $(PROFILE_CFLAGS) -l
endif

#To apply the profiler data itself!
ifneq (,$(NDK_PROFILE))
LOCAL_CFLAGS := $(PROFILE_CFLAGS) -DNDK_PROFILE $(LOCAL_CFLAGS)
LOCAL_STATIC_LIBRARIES := $(LOCAL_STATIC_LIBRARIES) android-ndk-profiler
endif
#end of profiler support block

include $(BUILD_SHARED_LIBRARY)
$(call import-module,SDL)
ifeq (1,$(use$(BUILD_SDL)_net))
$(call import-module,$(BUILD_SDL)_net)
endif

LOCAL_PATH := $(call my-dir)

#Apply profile support!
ifneq (,$(NDK_PROFILE))
#Below commented our searched NDK_MODULE_PATH for the folder name specified and includes it's Android.mk file for us. We do this manually as specified by our variable!
$(call import-add-path,$(NDK_PROFILE))
$(call import-module,android-ndk-profiler)
endif