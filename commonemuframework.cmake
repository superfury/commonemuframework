cmake_minimum_required(VERSION 3.24)

execute_process(COMMAND make -f Makefile.files ROOTPATH=. get-properties cfiles
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE CFILES)
execute_process(COMMAND make -f Makefile.files ROOTPATH=. get-properties buildname
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE BUILDNAME)
execute_process(COMMAND make -f Makefile.files ROOTPATH=. get-properties builddefine
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE BUILDDEFINE)

project(${BUILDNAME})

MESSAGE(STATUS "Build name: " ${BUILDNAME})

# Needed so that cmake uses our find modules.
list(APPEND CMAKE_MODULE_PATH ${CMAKE_BINARY_DIR})

find_package(SDL2 REQUIRED)

OPTION(USE_SDL2NET "SDL2_net Support" OFF)

IF( USE_SDL2NET )
	find_package(SDL2_net REQUIRED)
	IF( SDL2_net_FOUND )
		include (FindSDL2_net)
		target_include_libraries(${BUILDNAME} ${SDL2NET_INCLUDE_DIRS})
		add_compile_definitions(SDL2_net)
	ENDIF()
ENDIF()

add_compile_definitions(SDL2)
add_compile_definitions(${BUILDDEFINE})

include_directories(${SDL2_INCLUDE_DIRS})
include_directories(${CMAKE_SOURCE_DIR})
include_directories(${CMAKE_SOURCE_DIR}/../commonemuframework)

set(CMAKE_C_STANDARD 99)

add_executable(${BUILDNAME} ${CFILES})
#target_sources(${BUILDNAME} PUBLIC FILE_SET main BASE_DIRS ${CMAKE_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/../commonemuframework FILES ${CFILES})

# IF( SDL2_net_FOUND )
	target_link_libraries(${BUILDNAME} ${SDL2_LIBRARIES} ${SDL2NET_LIBRARIES})
# ELSE()
	target_link_libraries(${BUILDNAME} ${SDL2_LIBRARIES})
# ENDIF()

# IF( NOT SDL2_net_FOUND )
# ENDIF()