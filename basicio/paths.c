#include "headers/types.h" //Basic type support!
#include "headers/fopen64.h" //file support!
#include "headers/emu/emu_misc.h" //FILE_EXISTS support and common loading support!
#include "headers/emu/emucore.h" //Core support for paths!
#include "headers/support/zalloc.h" //Memory allocation support!

#ifdef IS_SWITCH
#include <sys/unistd.h> //Required for getcwd()
#endif

//What file to use for saving the BIOS!
#define DEFAULT_SETTINGS_FILE "SETTINGS.INI"
#define DEFAULT_REDIRECT_FILE "redirect.txt"
#define DEFAULT_ROOT_PATH "."

char settings_file[256] = DEFAULT_SETTINGS_FILE; //Our settings file!
char ROOTPATH_VAR[256] = DEFAULT_ROOT_PATH; //Our root path!

extern char ROOTPATH_VAR[256]; //Root path variable!
extern char settings_file[256]; //BIOS settings file to use!
extern char capturepath[256];
extern char logpath[256];

void BIOS_updateDirectories()
{
#if defined(IS_ANDROID) || defined(IS_LINUX) || defined(IS_VITA) || defined(IS_PS3) || defined(IS_SWITCH) || defined(IS_WINDOWS)
	safestrcpy(logpath, sizeof(logpath), ROOTPATH_VAR); //Root dir!
	safestrcat(logpath, sizeof(logpath), PATHSEPERATOR);
	//Base path loaded!
	safestrcpy(capturepath, sizeof(capturepath), logpath); //Clone!
	//Now, create the actual subdirs!
	safestrcat(capturepath, sizeof(capturepath), "captures");
	safestrcat(logpath, sizeof(logpath), "logs");
	//Now, all paths are loaded! Ready to run!
#endif
#ifdef APPSPECIFICDIRECTORIESUPDATE
	APPSPECIFICDIRECTORIESUPDATE(); //Update specific directories for the app!
#endif
}

int storage_strpos(char* str, char ch)
{
	int pos = 0;
	for (; (*str && (*str != ch)); ++str, ++pos); //Not found yet?
	if (*str == ch) return pos; //Found position!
	return -1; //Not found!
}

byte is_pathseperator(char c)
{
	if (c == '/') //First case?
	{
		return 1; //First case!
	}
	if (c == '\\') //Second case?
	{
		return 2; //Second case!
	}
	return 0; //Not a path seperator!
}

void recursive_mkdir(const char* dir) {
	char tmp[256];
	char* p = NULL;
	char backup;
	size_t len;

	snprintf(tmp, sizeof(tmp), "%s", dir);
	len = safestrlen(tmp, sizeof(tmp));
	if ((tmp[len - 1] == '/') || (tmp[len - 1] == '\\'))
		tmp[len - 1] = 0;
	for (p = tmp + 1; *p; p++)
		if (is_pathseperator(*p)) {
			backup = *p; //Path seperator used!
			*p = 0;
			domkdir(tmp);
			*p = backup; //Restore for further processing!
		}
	domkdir(tmp);
}

byte is_writablepath(char* path)
{
	char fullpath[256];
	BIGFILE* f;
	memset(&fullpath, 0, sizeof(fullpath)); //init!
	safestrcpy(fullpath, sizeof(fullpath), path); //Set the path!
	safestrcat(fullpath, sizeof(fullpath), PATHSEPERATOR); //Add directory seperator!
	safestrcat(fullpath, sizeof(fullpath), "writable.txt"); //test file!
	recursive_mkdir(path); //Try to create the folder, if available, first!
	f = emufopen64(fullpath, "wb");
	if (f)
	{
		emufclose64(f); //Close the file!
		delete_file(path, "writable.txt"); //Delete the file!
		return 1; //We're writable!
	}
	return 0; //We're not writable!
}

byte is_textcharacter(char c)
{
	if ((c >= 'a') && (c <= 'z')) return 1; //Text!
	if ((c >= 'A') && (c <= 'Z')) return 1; //Text!
	if ((c >= '0') && (c <= '9')) return 1; //Text!
	switch (c) //Remaining cases?
	{
	case '~':
	case '`':
	case '!':
	case '@':
	case '#':
	case '$':
	case '%':
	case '^':
	case '&':
	case '*':
	case '(':
	case ')':
	case '-':
	case '_':
	case '+':
	case '=':
	case '{':
	case '}':
	case '[':
	case ']':
	case '|':
	case '\\':
	case ':':
	case ';':
	case '"':
	case '\'':
	case '<':
	case ',':
	case '>':
	case '.':
	case '?':
		return 2; //Valid text for a filename to end in!
	default:
		break;
	}
	return 0; //Not a text character!
}

//result: -1:invalid, 0:Relative, 1:absolute, 2:URL
sbyte is_absolutepath(char* path)
{
	size_t len;
	#ifdef IS_PSP
	size_t len2;
	char internalstorage1identifier[6] = "flash";
	char internalstorage2identifier[6] = "ms0:/";
	#endif
	char URIidentifier[3] = ":/";
	#ifdef IS_WINDOWS
	//Windows drive identifier!
	char driveidentifier[3] = ":\\";
	#endif

	byte URIidentifier_len;
	#ifdef IS_WINDOWS
	byte driveidentifier_len;
	#endif
	char* p;
	if (!path) return -1; //Nothing!
	if (!*path) return -1; //Invalid path!
	#if !defined(IS_WINDOWS) && !defined(IS_PSP)
	if (is_pathseperator(*path)) //Starts with a path seperator?
	{
		return 1; //Plain absolute path!
	}
	#else
	//Windows and PSP don't have linux-style root paths?
	if (is_pathseperator(*path)) //Starts with a path seperator?
	{
		if (is_pathseperator(*path) == 1) //Invalid to use on Windows?
		{
			return -1; //Invalid path!
		}
		return 1; //Plain absolute path!
	}
	#endif
	//Now, check for URI header!
	p = path; //Init!
	len = strlen(path); //Length of the string!
	#ifdef IS_PSP
	len2 = len; //Copy to check!
	#endif
	URIidentifier_len = safe_strlen(URIidentifier, sizeof(URIidentifier)); //Length to check against!
	#ifdef IS_WINDOWS
	driveidentifier_len = safe_strlen(driveidentifier, sizeof(driveidentifier)); //Length to check against!
	#endif
	for (; *p; ++p, --len) //Check until EOS!
	{
		if ((p != path) && (len >= URIidentifier_len)) //Might start an URI?
		{
			if (memcmp(p, &URIidentifier,URIidentifier_len)==0) //Matched?
			{
				#ifdef IS_PSP
				if (len2 >= safe_strlen(internalstorage1identifier, sizeof(internalstorage1identifier))) //Check #1!
				{
					if (memcmp(path, &internalstorage1identifier, safe_strlen(internalstorage1identifier, sizeof(internalstorage1identifier))) == 0) //Matched?
					{
						return 1; //Absolute drive path!
					}
				}
				if (len2 >= safe_strlen(internalstorage2identifier, sizeof(internalstorage2identifier))) //Check #1!
				{
					if (memcmp(path, &internalstorage2identifier, safe_strlen(internalstorage2identifier, sizeof(internalstorage2identifier))) == 0) //Matched?
					{
						return 1; //Absolute drive path!
					}
				}
				#endif
				//Found an proper URL header!
				return 2; //Absolute URL path!
			}
			//Otherwise, not matched yet, check for other options!
		}
		#ifdef IS_WINDOWS
		//Windows absolute path?
		if ((p == (path + 1)) && (len >= driveidentifier_len)) //Might start an URI?
		{
			if (memcmp(p, &driveidentifier, driveidentifier_len) == 0) //Matched?
			{
				//Found an proper absolute drive path header!
				return 1; //Absolute drive path!
			}
			//Otherwise, not matched yet, check for other options!
		}
		#endif
		if (is_textcharacter(*p) != 1) //Non-text is invalid for URL header?
		{
			break; //Abort search: invalid header!
		}
	}
	return 0; //Relative path!
}

byte loadRedirectFile(char* filename, char *path, byte* is_redirected, char* redirectdir, size_t redirectdir_size)
{
#ifdef LOG_REDIRECT
	char temppath[256];
	memset(&temppath, 0, sizeof(temppath)); //Set log path!
	safestrcpy(temppath, sizeof(temppath), logpath); //Set log path!
	safestrcat(temppath, sizeof(temppath), PATHSEPERATOR); //Inside the directory!
	safestrcat(temppath, sizeof(temppath), DEFAULT_REDIRECT_FILE); //Log file for testing!
	char buffer[256];
	FILE* f2;
#endif
	char redirectpath[256]; //A path!
	char redirectpath2[256];
	int_64 redirectdirsize; //How much has been read?
	BIGFILE* f; //File to use!
	if (file_exists(filename) && (*is_redirected == 0)) //Redirect for main directory?
	{
#ifdef LOG_REDIRECT
		memset(&buffer, 0, sizeof(buffer)); //Init buffer!
		snprintf(buffer, sizeof(buffer), "Attempting redirect...");
		f2 = emufopen64(temppath, "ab"); //Log the filename!
		if (f2) //Valid?
		{
			emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
			emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
			emufclose64(f2); //Close!
		}
#endif
		f = emufopen64(filename, "rb");
		if (f) //Valid?
		{
#ifdef LOG_REDIRECT
			memset(&buffer, 0, sizeof(buffer)); //Init buffer!
			snprintf(buffer, sizeof(buffer), "Valid file!");
			f2 = emufopen64(temppath, "ab"); //Log the filename!
			if (f2) //Valid?
			{
				emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
				emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
				emufclose64(f2); //Close!
			}
#endif
			emufseek64(f, 0, SEEK_END); //Goto EOF!
			if (((redirectdirsize = emuftell64(f)) < sizeof(redirectpath)) && redirectdirsize) //Valid to read?
			{
#ifdef LOG_REDIRECT
				memset(&buffer, 0, sizeof(buffer)); //Init buffer!
				snprintf(buffer, sizeof(buffer), "Valid size!");
				f2 = emufopen64(temppath, "ab"); //Log the filename!
				if (f2) //Valid?
				{
					emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
					emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
					emufclose64(f2); //Close!
				}
#endif
				emufseek64(f, 0, SEEK_SET); //Goto BOF!
				memset(&redirectpath, 0, redirectdir_size); //Clear for our result to be stored safely!
				if (emufread64(&redirectpath, 1, redirectdirsize, f) == redirectdirsize) //Read?
				{
#ifdef LOG_REDIRECT
					memset(&buffer, 0, sizeof(buffer)); //Init buffer!
					snprintf(buffer, sizeof(buffer), "Valid content!");
					f2 = emufopen64(temppath, "ab"); //Log the filename!
					if (f2) //Valid?
					{
						emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
						emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
						emufclose64(f2); //Close!
					}
#endif
					for (; safestrlen(redirectpath, redirectdir_size);) //Valid to process?
					{
						switch (redirectpath[safestrlen(redirectpath, redirectdir_size) - 1]) //What is the final character?
						{
						case '/': //Invalid? Take it off!
						case '\\': //Invalid? Take it off!
							if (safestrlen(redirectpath, redirectdir_size) > 1) //More possible? Check for special path specification(e.g. :// etc.)!
							{
								if (!is_textcharacter(redirectpath[safestrlen(redirectpath, redirectdir_size) - 2])) //Not normal path?
								{
									redirectpath[safestrlen(redirectpath, redirectdir_size) - 1] = '\0'; //Take it off, we're specifying the final slash ourselves!
									goto redirect_validpath;
								}
							}
							//Invalid normal path: handle normally!
						case '\n':
						case '\r':
							redirectpath[safestrlen(redirectpath, redirectdir_size) - 1] = '\0'; //Take it off!
							break;
						default:
						redirect_validpath: //Apply valid directory for a root domain!
#ifdef LOG_REDIRECT
							memset(&buffer, 0, sizeof(buffer)); //Init buffer!
							snprintf(buffer, sizeof(buffer), "Trying: Redirecting to: %s", redirectpath); //Where are we redirecting to?
							f2 = emufopen64(temppath, "ab"); //Log the filename!
							if (f2) //Valid?
							{
								emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
								emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
								emufclose64(f2); //Close!
							}
#endif
							//Check for type!
							if (is_absolutepath(&redirectpath[0]) < 0) //Invalid path?
							{
								goto invalidpath; //Invalid path to load!
							}
							if (!is_absolutepath(&redirectpath[0])) //Relative path?
							{
								if ((safe_strlen(redirectpath,sizeof(redirectpath))+safe_strlen(redirectpath2,sizeof(redirectpath2)))>=sizeof(redirectpath)) //Combined too long?
								{
									goto invalidpath; //Invalid path to load!
								}
								//Convert from relative to absolute path!
								memset(&redirectpath2, 0, sizeof(redirectpath2)); //Init!
								safe_strcpy(&redirectpath2[0], sizeof(redirectpath2), path); //Base path!
								safe_strcat(&redirectpath2[0], sizeof(redirectpath2), PATHSEPERATOR); //Path seperator!
								safe_strcat(&redirectpath2[0], sizeof(redirectpath2), redirectpath);
								safe_strcpy(&redirectpath[0],sizeof(redirectpath), redirectpath2); //Rewrite the relative path into an absolute path to use!
							}

							if (is_writablepath(redirectpath)) //Writable path?
							{
								if (safe_strlen(redirectpath, sizeof(redirectpath)) >= redirectdir_size) //Invalid to use?
								{
									goto invalidpath; //invalid path to load!
								}
								*is_redirected = 1; //We're redirecting!
								safe_strcpy(redirectdir, redirectdir_size, redirectpath); //Make active instead!
								emufclose64(f); //Stop checking!
								return 1; //Redirected!
							}
							//If not writable, abort cleanly!
							goto invalidpath; //Invalid path!
							break;
						}
					}
				}
			}
			invalidpath:
			emufclose64(f); //Stop checking!
		}
	}
	return 0; //Not redirected!
}

void checkRootDirRedirect(byte* is_redirected, char* redirectdir, size_t redirectdir_size)
{
	#ifdef IS_ANDROID
	char path[256];
	char path2[256];
	char* start;
	if (safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR)) >= strlen("/Android/data/" ANDROID_APP_DIR "/files")) //Long enough to might reveal the root directory?
	{
		memset(&path, 0, sizeof(path)); //Init!
		if (redirectdir_size <= sizeof(path)) //Safe to use?
		{
			safestrcpy(path, sizeof(path), ROOTPATH_VAR); //Copy the directory!
			start = &path[safestrlen(path, sizeof(path)) - strlen("/Android/data/" ANDROID_APP_DIR "/files")]; //Get the root path if possible!
			if (strcmp(start, "/Android/data/" ANDROID_APP_DIR "/files")==0) //Root path found?
			{
				*start = 0; //Take the root path of the storage!
				memset(&path2, 0, sizeof(path2)); //Init!
				safe_strcpy(path2, sizeof(path2), path); //Copy!
				safestrcat(path, sizeof(path), PATHSEPERATOR APP_ROOT_DIR PATHSEPERATOR DEFAULT_REDIRECT_FILE ); //Redirect file location to read!
				safestrcat(path2, sizeof(path2), PATHSEPERATOR APP_ROOT_DIR); //Redirect file directory!
				loadRedirectFile(&path[0], &path2[0], is_redirected, redirectdir, redirectdir_size); //Try and run any redirection if specified!
			}
		}
	}
	#endif
}

void BIOS_DetectStorage() //Auto-Detect the current storage to use, on start only!
{
#ifdef IS_ANDROID
#ifdef SDL3
	Uint32 externalstoragestate;
#else
	//SDL2?
	int externalstoragestate;
#endif
#endif
#if defined(IS_ANDROID) || defined(IS_LINUX) || defined(IS_VITA) || defined(IS_PS3) || defined(IS_SWITCH) || defined(IS_WINDOWS)
#ifndef IS_ANDROID
#if defined(SDL2) || defined(SDL3)
	char* base_path;
	#ifndef SDL3
	char* linuxpath = SDL_getenv(ENVNAME);
	#else
	const char *linuxpath = SDL_getenv(ENVNAME);
	#endif
	if (linuxpath) //Linux environment path specified?
	{
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), linuxpath);
		//SDL_free(linuxpath); //Release it, now that we have it! For some reason, this cannot be done on a linux version?
		if (safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR)) > 1) //Valid length?
		{
			if (is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)])) //Ending with a slash? Check to strip!
			{
				if (!is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 2)])) //Not ending with a double slash? Valid to strip!
				{
					ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)] = '\0'; //Strip off the trailing slash!
				}
			}
		}
		else //Invalid length? Fallback!
		{
			if (strcmp(ROOTPATH_VAR, ".") != 0) //Not CWD?
			{
				goto handleLinuxBasePathSDL2_3; //Fallback!
			}
		}
	}
	else
	{
	handleLinuxBasePathSDL2_3: //Fallback!
#ifndef IS_WINDOWS
		base_path = SDL_GetPrefPath(APP_PRODUCER, APP_NAME);
#else
		base_path = NULL; //CWD on Windows!
#endif
		if (base_path) //Gotten?
		{
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), base_path);
			SDL_free(base_path); //Release it, now that we have it!
			if (safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR)) > 1) //Valid length?
			{
				if (is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)])) //Ending with a slash? Check to strip!
				{
					if (!is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 2)])) //Not ending with a double slash? Valid to strip!
					{
						ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)] = '\0'; //Strip off the trailing slash!
					}
				}
			}
			else //Default length?
			{
#ifdef IS_LINUX
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "~/" APP_ROOT_DIR); //Default path!
#else
#if defined(IS_VITA) || defined(IS_PS3)
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "/dev_hdd0/game/" APP_ID "/USRDIR/files"); //CWD!
#else
#ifdef IS_SWITCH
				if (getcwd(&ROOTPATH_VAR[0], sizeof(ROOTPATH_VAR)) == NULL)
#endif
					safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "."); //CWD!
#endif
#endif
			}
		}
		else //Fallback to default path?
		{
#ifdef IS_LINUX
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "~/" APP_ROOT_DIR);
#else
#ifdef IS_PS3
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR),  "/dev_hdd0/game/" APP_ID "/USRDIR/files"); //CWD!
#else
#ifdef IS_VITA
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "ux0:/data/" APP_PRODUCER "/" APP_ROOT_DIR); //CWD!
#else
#ifdef IS_SWITCH
			if (getcwd(&ROOTPATH_VAR[0], sizeof(ROOTPATH_VAR)) == NULL)
#endif
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "."); //CWD!
#endif
#endif
#endif
		}
	}
#else
	char* linuxpath = SDL_getenv(ENVNAME);
	if (linuxpath) //Linux path specified?
	{
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), linuxpath);
		//SDL_free(linuxpath); //Release it, now that we have it! This doesn't seem to work on linux versions of SDL?
		if (safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR)) > 1) //Valid length?
		{
			if (is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)])) //Trailing slash?
			{
				if (!is_pathseperator(ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 2)])) //Not ending with a double slash? Valid to strip!
				{
					ROOTPATH_VAR[safestrlen(ROOTPATH_VAR, sizeof(ROOTPATH_VAR) - 1)] = '\0'; //Strip off the trailing slash!
				}
			}
		}
		else //Invalid length? Fallback!
		{
			if (strcmp(ROOTPATH_VAR, ".") != 0) //Not CWD?
			{
#if defined(IS_LINUX) || defined(IS_PS3)
				goto handleLinuxBasePathSDL; //Fallback!
#endif
			}
		}
	}
	else
	{
#if defined(IS_LINUX) || defined(IS_PS3)
		handleLinuxBasePathSDL:
#endif
		//SDL1.2.x on linux?
#ifdef IS_LINUX
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "~/" APP_ROOT_DIR);
#else
#ifdef IS_PS3
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR),  "/dev_hdd0/game/" APP_ID "/USRDIR/files"); //CWD!
#else
#ifdef IS_VITA
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "ux0:/data/" APP_PRODUCER "/" APP_ROOT_DIR); //CWD!
#else
#ifdef IS_SWITCH
		if (getcwd(&ROOTPATH_VAR[0], sizeof(ROOTPATH_VAR)) == NULL)
#endif
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), "."); //CWD!
#endif
#endif
#endif
	}
#endif
#endif

#ifdef IS_SWITCH
	if (strcmp(ROOTPATH_VAR, PATHSEPERATOR) == 0) //Invalid path to use?
	{
		safe_strcat(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), APP_PRODUCER "/" APP_ROOT_DIR); //Proper directory to use!
	}
#endif

	byte is_redirected = 0;
	is_redirected = 0; //Init redirect status for main directory!
	//Try external media first!
	#ifdef SDL3
	#if SDL_VERSION_ATLEAST(3,1,3)
	const char* environment;
	#else
	char* environment;
	#endif
	#else
	char* environment;
	#endif
	char *environment2, *environment3;
	int multipathseperator; //Multi path seperator position in the current string! When set, it's changed into a NULL character to read out the current path in the list!

	char redirectdir[256]; //Redirect directory!

#ifdef PELYAS_SDL
	if (environment = getenv("SECONDARY_STORAGE")) //Autodetected try secondary storage?
#else
#ifdef IS_ANDROID
	if ((environment = SDL_getenv("SECONDARY_STORAGE")) != NULL) //Autodetected try secondary storage?
#else
	if (0) //Don't use on non-Android!
#endif
#endif
	{
		if (environment == NULL) goto scanDefaultpath; //Start scanning the default path, nothing found!
		if (*environment == '\0') goto scanDefaultpath; //Start scanning the default path, nothing found!
		environment2 = zalloc(strlen(environment)+1,"tempenv",NULL); //Allocate buffer!
		if (environment2 == NULL) goto scanDefaultpath; //Start scanning the default path, nothing usable!
		safestrcpy(environment2,strlen(environment),environment); //Create an environment we can modify!
		environment3 = environment2; //Where to start looking! Start at the beginning of the string!
		scanNextSecondaryPath:
		if ((multipathseperator = storage_strpos(environment3, ':')) != -1) //Multiple environments left to check?
		{
			environment3[multipathseperator] = '\0'; //Convert the seperator into an EOS for reading the current value out!
		}
		//Check the currently loaded path for writability!
		if (is_writablepath(environment3)) //Writable?
		{
			safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), environment); //Root path of the disk!
			if (multipathseperator != -1) //To revert multiple path seperator?
			{
				environment3[multipathseperator] = ':'; //Restore the path seperator from the EOS!
			}
			//Root directory loaded!
			safestrcat(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), PATHSEPERATOR APP_ROOT_DIR); //Our storage path!
			domkdir(ROOTPATH_VAR); //Make sure to create our parent directory, if needed!
			safestrcat(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), PATHSEPERATOR "files"); //Subdirectory to store the files!
			freez((void **)&environment2,strlen(environment)+1,"tempenv"); //Free!
			goto finishpathsetting;
		}
		//To check the next path?
		if (multipathseperator != -1) //To revert multiple path seperator?
		{
			environment3[multipathseperator] = ':'; //Restore the path seperator from the EOS!
			environment3 += (multipathseperator + 1); //Skip past the multiple path seperator!
		}
		else
		{
			freez((void **)&environment2,strlen(environment)+1,"tempenv"); //Free!
			goto scanDefaultpath; //Finished scanning without multiple paths left!
		}
		goto scanNextSecondaryPath; //Scan the next path in the list!
	}

scanDefaultpath:
#ifdef IS_ANDROID
	//Android changes the root path!
#ifdef PELYAS_SDL
	if ((environment = getenv("SDCARD")) != NULL)
	{
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), environment); //path!
		safestrcat(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), PATHSEPERATOR "Android/data/" ANDROID_APP_DIR "/files");
	}
#else
	if ((environment = SDL_getenv("SDCARD")) != NULL) //Autodetected?
	{
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), environment); //path!
		safestrcat(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), PATHSEPERATOR "Android/data/" ANDROID_APP_DIR "/files");
	}
	else
	{
#ifndef SDL3
		externalstoragestate = SDL_AndroidGetExternalStorageState(); //Directly!
#else
		//SDL3?
		#if SDL_VERSION_ATLEAST(3,1,3)
		externalstoragestate = SDL_GetAndroidExternalStorageState(); //Get the storage state!
		#else
		#if SDL_VERSION_ATLEAST(3,1,2)
		if (SDL_GetAndroidExternalStorageState(&externalstoragestate) != 0) //Fail?
		{
			externalstoragestate = 0; //No access!
		}
		#else
		if (SDL_AndroidGetExternalStorageState(&externalstoragestate) != 0) //Fail?
		{
			externalstoragestate = 0; //No access!
		}
		#endif
		#endif
#endif
		if (externalstoragestate == (SDL_ANDROID_EXTERNAL_STORAGE_WRITE | SDL_ANDROID_EXTERNAL_STORAGE_READ)) //External settings exist?
		{
			#ifndef SDL3
			if (SDL_AndroidGetExternalStoragePath()) //Try external.
			#else
			#if SDL_VERSION_ATLEAST(3,1,2)
			if (SDL_GetAndroidExternalStoragePath()) //Try external.
			#else
			if (SDL_AndroidGetExternalStoragePath()) //Try external.
			#endif
			#endif
			{
				#ifndef SDL3
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetExternalStoragePath()); //External path!
				#else
				#if SDL_VERSION_ATLEAST(3,1,2)
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_GetAndroidExternalStoragePath()); //External path!
				#else
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetExternalStoragePath()); //External path!
				#endif
				#endif
			}
			#ifndef SDL3
			else if (SDL_AndroidGetInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetInternalStoragePath()); //Internal path!
			}
			#else
			#if SDL_VERSION_ATLEAST(3,1,2)
			else if (SDL_GetAndroidInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_GetAndroidInternalStoragePath()); //Internal path!
			}
			#else
			else if (SDL_AndroidGetInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetInternalStoragePath()); //Internal path!
			}
			#endif
			#endif
		}
		else
		{
			#ifndef SDL3
			if (SDL_AndroidGetInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetInternalStoragePath()); //Internal path!
			}
			#else
			#if SDL_VERSION_ATLEAST(3,1,2)
			if (SDL_GetAndroidInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR),
				 SDL_GetAndroidInternalStoragePath()); //Internal path!
			}
			#else
			if (SDL_AndroidGetInternalStoragePath()) //Try internal.
			{
				safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), SDL_AndroidGetInternalStoragePath()); //Internal path!
			}
			#endif
			#endif
		}
	}
#endif
#endif

finishpathsetting:
	memset(&redirectdir, 0, sizeof(redirectdir)); //Init!
	checkRootDirRedirect(&is_redirected,&redirectdir[0], sizeof(redirectdir)); //Check for any root directory redirect!
	safestrcpy(settings_file, sizeof(settings_file), ROOTPATH_VAR); //Our settings file location!
	safestrcat(settings_file, sizeof(settings_file), PATHSEPERATOR); //Inside the directory!
	safestrcat(settings_file, sizeof(settings_file), DEFAULT_SETTINGS_FILE); //Our settings file!
	//Normal devices? Don't detect!

	//Check for redirection to apply!
	if (is_redirected == 0) //Not redirected already?
	{
		safestrcpy(redirectdir, sizeof(redirectdir), ROOTPATH_VAR); //Check for redirects!
		safestrcat(redirectdir, sizeof(redirectdir), PATHSEPERATOR); //Inside the directory!
		safestrcat(redirectdir, sizeof(redirectdir), DEFAULT_REDIRECT_FILE); //Our redirect file!
		#ifdef LOG_REDIRECT
		char temppath[256];
		memset(&temppath, 0, sizeof(temppath)); //Set log path!
		safestrcpy(temppath, sizeof(temppath), logpath); //Set log path!
		safestrcat(temppath, sizeof(temppath), PATHSEPERATOR); //Inside the directory!
		safestrcat(temppath, sizeof(temppath), DEFAULT_REDIRECT_FILE); //Log file for testing!
		char buffer[256];
		FILE* f2;
		char lb[2] = { 0xD,0xA }; //Line break!
		memset(&buffer, 0, sizeof(buffer)); //Init buffer!
		snprintf(buffer, sizeof(buffer), "Redirect file: %s", redirectdir);
		f2 = emufopen64(temppath, "wb"); //Log the filename!
		if (f2) //Valid?
		{
			emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
			emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
			emufclose64(f2); //Close!
		}
		#endif
		loadRedirectFile(&redirectdir[0], &ROOTPATH_VAR[0], &is_redirected, &redirectdir[0], sizeof(redirectdir)); //Perform a normal redirect if possible!
	}

#ifdef LOG_REDIRECT
	dolog("redirect", "Content:%s/%i!", redirectdir, is_redirected);
#endif
	if (is_redirected && redirectdir[0]) //To redirect?
	{
		safestrcpy(ROOTPATH_VAR, sizeof(ROOTPATH_VAR), redirectdir); //The new path to use!
#ifdef LOG_REDIRECT
		memset(&buffer, 0, sizeof(buffer)); //Init buffer!
		snprintf(buffer, sizeof(buffer), "Redirecting to: %s", ROOTPATH_VAR); //Where are we redirecting to?
		f2 = emufopen64(temppath, "ab"); //Log the filename!
		if (f2) //Valid?
		{
			emufwrite64(&buffer, 1, safestrlen(buffer, sizeof(buffer)), f2); //Log!
			emufwrite64(&lb, 1, sizeof(lb), f2); //Line break!
			emufclose64(f2); //Close!
		}
#endif
		goto finishpathsetting; //Go and apply the redirection!
	}

	//Finally, after all checks are complete, actually create the used root directory and update all paths!
	recursive_mkdir(ROOTPATH_VAR); //Auto-create our root directory!
	BIOS_updateDirectories(); //Update all directories!
#endif
}