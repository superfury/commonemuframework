/*

Copyright (C) 2019 - 2022 Superfury

This file is part of The Common Emulator Framework.

The Common Emulator Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Common Emulator Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Common Emulator Framework.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "headers/types.h" //Basic types!
#include "headers/support/zalloc.h" //Zero allocation support!
#include "headers/fopen64.h" //Our own types!
#include "headers/support/locks.h" //Locking support!

//Compatibility with SDL 1.2 builds!
#ifndef RW_SEEK_SET
#define RW_SEEK_SET SEEK_SET
#endif

#ifndef RW_SEEK_CUR
#define RW_SEEK_CUR SEEK_CUR
#endif

#ifndef RW_SEEK_END
#define RW_SEEK_END SEEK_END
#endif

#if defined(IS_LINUX) && !defined(IS_ANDROID)
#include <fcntl.h>
#include <sys/types.h>
//Remove sleep, as it's defined by unistd.h
#undef sleep
#include <unistd.h>
#endif

BIGFILE* lastopenedfile = NULL; //Last opened file!

/*

This is a custom PSP&PC library for adding universal 64-bit fopen support to the project using platform-specific calls.

*/

byte emufileopened(char* filename)
{
	BIGFILE* f;
	byte result;
	lock(LOCK_FILES);
	result = 0; //Not found!
	f = lastopenedfile;
	for (;f;f=f->next) //Left?
	{
		if (strcmp(f->filename, filename) == 0) //Found?
		{
			result = 1; //Found!
			break; //Stop searching!
		}
	}
	unlock(LOCK_FILES);
	return result; //Give the result!
}

int64_t emuftell64(BIGFILE *stream)
{
	if (!stream) return -1LL; //Error!
	BIGFILE *b = (BIGFILE *)stream; //Convert!
	if (b->ready==0) return -1LL; //Error when not ready!
	return b->position; //Our position!
}

int emufeof64(BIGFILE *stream)
{
	if (!stream) return 1; //EOF!
	BIGFILE *b = (BIGFILE *)stream; //Convert!
	if (b->ready==0) return -1; //Error when not ready!
	return (b->position >= b->size) ? 1 : 0; //Our eof marker is set with non-0 values!
}

int emufseek64(BIGFILE *stream, int64_t pos, int direction)
{
	int result;
	if (!stream) return -1; //Error!
	BIGFILE *b = (BIGFILE *)stream; //Convert!
	if (b->ready==0) return -1; //Error when not ready!
#ifndef IS_PS3
	int_64 resposition;
	int whence;
#else
	//PS3?
	u64 resposition;
	s32 whence;
#endif
	switch (direction)
	{
	case SEEK_CUR:
		#ifdef IS_PS3
		whence = SEEK_CUR;
		#else
		#ifdef SDL3
		whence = SDL_IO_SEEK_CUR;
		#else
		whence = RW_SEEK_CUR;
		#endif
		#endif
		break;
	case SEEK_END:
		#ifdef IS_PS3
		whence = SEEK_END;
		#else
		#ifdef SDL3
		whence = SDL_IO_SEEK_END;
		#else
		whence = RW_SEEK_END;
		#endif
		#endif
		break;
	case SEEK_SET:
		#ifdef IS_PS3
		whence = SEEK_SET;
		#else
		#ifdef SDL3
		whence = SDL_IO_SEEK_SET;
		#else
		whence = RW_SEEK_SET;
		#endif
		#endif
		break;
	default: //unknown?
		return -1; //Error!
		break;
	}
#ifdef IS_PS3
	resposition = pos; //Where are we requesting to go?
	if (sysLv2FsLSeek64(b->f,resposition,whence,&resposition)>=0) //Direction is constant itself!
#else
#ifdef SDL3
	if ((resposition = (int_64)SDL_SeekIO(b->f, pos, whence)) >= 0) //Direction is constant itself!
#else
	if ((resposition = (int_64)SDL_RWseek(b->f, pos, whence))>=0) //Direction is constant itself!
#endif
#endif
	{
		b->position = resposition; //Use our own position indicator!
		result = 0; //OK!
	}
	else
	{
#ifndef IS_PS3
#ifdef SDL3
		resposition = (int_64)SDL_TellIO(b->f); //Where are we?
#else
		resposition = (int_64)SDL_RWtell(b->f); //Where are we?
#endif
#endif
		#ifdef IS_WINDOWS
		if (resposition < 0) //Invalid position?
		{
			resposition &= 0xFFFFFFFFU; //Mask off invalid bits!
		}
		#endif
		//Assume what we got from RWtell is the actual current position!
		b->position = resposition; //The current position!
		result = 0; //OK!
		if ((b->position != pos) && (direction == SEEK_SET)) //Failed to seek absolute?
		{
			result = -1; //Failed after all!
		}
	}
	return result; //Give the result!
}

int emufflush64(BIGFILE *stream)
{
	if (!stream) return 1; //EOF!
	return 0; //Ignore: not supported!
}

void BIGFILEdealloc(void** ptr, uint_64 size, semaphore_type* lock)
{
	BIGFILE** f;
	BIGFILE* f2;
	f = (BIGFILE**)ptr; //The wave file pointer
	if (f) //valid?
	{
		f2 = *f; //Get the pointer value!
		if (f2) //Valid pointer?
		{
			emufclose64(f2); //Close the file!
			f2 = NULL;
		}
		if (f2 == NULL) //Already closed?
		{
			return; //Already handled!
		}
	}
	DEALLOCFUNC defaultdealloc = getdefaultdealloc(); //Default deallocation function!
	defaultdealloc(ptr, size, NULL); //Release the pointer normally by direct deallocation!
}

BIGFILE *emufopen64(char *filename, char *mode)
{
	char newmode[256];
	char c[2];
	byte isappend;
	Sint64 pos;
	char *curmode;
	BIGFILE *stream;
	#ifdef IS_PS3
	u32 filemode;
	byte is_writable;
	byte is_readable;
	u64 filepos;
	#endif
	if (!filename) //Invalid filename?
	{
		return NULL; //Invalid filename!
	}
	if (!mode) //Invalid mode?
	{
		return NULL; //Invalid mode!
	}
	if (!safe_strlen(filename, 255)) //Empty filename?
	{
		return NULL; //Invalid filename!
	}

	stream = (BIGFILE *)zalloc(sizeof(BIGFILE), "BIGFILE", getLock(LOCK_FILES)); //Allocate the big file!
	if (!stream) return NULL; //Nothing to be done!

	char *modeidentifier = mode; //First character of the mode!
	if (strcmp(modeidentifier, "") == 0) //Nothing?
	{
		freez((void **)&stream, sizeof(BIGFILE), "Unused BIGFILE");
		return NULL; //Failed!
	}

	isappend = 0;
	curmode = mode;
	for (; *curmode; ++curmode) //Check the mode!
	{
		if (*curmode == 'a')
		{
			isappend = 1; //We're appending!
		}
	}
	memset(&newmode, 0, sizeof(newmode)); //Init!
	if (isappend)
	{
		c[0] = 0;
		c[1] = 0;
		curmode = mode;
		for (; *curmode; ++curmode)
		{
			if ((*curmode != 'a') && (*curmode!='+') && (*curmode!='r')) //Valid for appending?
			{
				c[0] = *curmode; //Use us!
				safestrcat(newmode,sizeof(newmode),c); //Convert to a normal read/write option instead!
			}
			else if (*curmode == 'a') //Append?
			{
				safestrcat(newmode,sizeof(newmode),"r+"); //Read and update rights!
			}
		}
		mode = &newmode[0]; //Use the new mode instead!
	}
	stream->isappending = isappend; //Are we appending?
#ifdef IS_PS3
	filemode = 0; //Initialize the mode!
	is_writable = 0; //Default: not writable.
	is_readable = 0; //Default: not readable.
	curmode = mode; //Mode to parse!
	for (;*curmode;++curmode)
	{
		switch (*curmode) //What mode to use?
		{
		case 'r': //Read?
			//Nothing special.
			is_readable = 1; //Readable!
			break;
		case 'w': //Write?
			filemode |= SYS_O_CREAT | SYS_O_TRUNC; //Create and truncate!
			is_writable = 1; //Writable!
		case 'a': //Append?
			is_writable = 1; //Writable!
			break; //Ignore for building the flags.
		case 'b': //ingored?
			break;
		case '+': //Update?
			is_writable = 1; //Writable!
			break;
		default: //Unknown identifier?
			goto erroroutfopen; //Error out if unknown identifiers are caught!
			break;
		}
	}

	if (stream->isappending) //Append too?
	{
		filemode |= SYS_O_APPEND; //Append!
	}
	if (!(is_readable|is_writable)) //Not readable or writable?
	{
		goto erroroutfopen;
	}
	if (is_readable && is_writable) //Read/write?
	{
		filemode |= SYS_O_RDWR; //Both readable and writable.
	}
	else if (is_readable) //Read-only?
	{
		filemode |= SYS_O_RDONLY; //Read-only!
	}
	else //Write-only?
	{
		filemode |= SYS_O_WRONLY; //Write-only!
	}
	if (sysLv2FsOpen(filename, filemode, &stream->f, 0777, NULL, 0)<0) //Failed to open?
	{
		erroroutfopen:
		freez((void **)&stream, sizeof(*stream), "fopen@InvalidStream"); //Free it!
		return NULL; //Failed!
	}
#else
#ifdef SDL3
	stream->f = SDL_IOFromFile(filename, mode); //Just call fopen!
#else
	stream->f = SDL_RWFromFile(filename, mode); //Just call fopen!
#endif
	if (stream->f==NULL) //Failed?
#endif
	{
		freez((void **)&stream, sizeof(*stream), "fopen@InvalidStream"); //Free it!
		return NULL; //Failed!
	}
	stream->ready = 1; //Stream is ready for use itself!

	memset(&stream->filename,0,sizeof(stream->filename)); //Init filename buffer!
	safestrcpy(&stream->filename[0],sizeof(stream->filename),filename); //Set the filename!

	//Detect file size!
	#if !defined(IS_PSP) && !defined(IS_PS3)
	#if defined(SDL2) || defined(SDL3)
	#ifdef SDL3
	stream->size = SDL_GetIOSize(stream->f); //This is the size!
	#else
	stream->size = SDL_RWsize(stream->f); //This is the size!
	#endif
	//Otherwise, detect manually for SDL v1!
	#else
	stream->size = 0; //Unknown size, determine later!
	#endif
	#endif
	#ifdef IS_PS3
	filepos = 0; //Init to no movement!
	if (sysLv2FsLSeek64(stream->f,0,SEEK_CUR,&filepos)>=0) //Position detected?
	{
		pos = filepos; //Where we are in the stream!
	}
	#else
	#ifdef SDL3
	pos = SDL_TellIO(stream->f); //What do we start out as?
	#else
	pos = SDL_RWtell(stream->f); //What do we start out as?
	#endif
	#endif
	stream->position = 0; //Default to position 0!
	if (pos > 0) //Valid position to use?
	{
		stream->position = pos;
		if (stream->size < 0) //Invalid size?
		{
			stream->size = pos; //Also the size(we're at EOF always)!
		}
	}
	else if (pos < 0) //Invalid position to use?
	{
		if (!emufseek64(stream, 0, SEEK_END)) //Goto EOF succeeded?
		{
			emufseek64(stream, 0, SEEK_SET); //Goto BOF!
		}
	}
#if (!defined(IS_PSP) && !defined(SDL2) && !defined(SDL3) && !defined(IS_PS3))
	//Determine the size for non-PSP SDL 1.2 builds!
	if (emufseek64(stream, 0, SEEK_END) >= 0) //Goto EOF!
	{
		stream->size = emuftell64(stream); //The file size!
		emufseek64(stream, pos, SEEK_SET); //Return to where we came from!
	}
	else
	{
		stream->size = 0; //Unknown size, assume 0!
	}
#endif
#if defined(IS_PSP) || defined(IS_PS3)
	if (emufseek64(stream, 0, SEEK_END) >= 0) //Goto EOF!
	{
		stream->size = emuftell64(stream); //The file size!
		emufseek64(stream, pos, SEEK_SET); //Return to where we came from!
	}
	else
	{
		stream->size = 0; //Unknown size, assume 0!
	}
#endif
	if (isappend) //Appending by default?
	{
		emufseek64(stream, 0, SEEK_END); //Goto EOF!
	}
	lock(LOCK_FILES);
	stream->next = NULL; //No next!
	if (lastopenedfile) //Last file present?
	{
		lastopenedfile->next = stream; //We're the next file!
		stream->prev = lastopenedfile; //Previous file in the link!
	}
	else //New last file?
	{
		lastopenedfile = stream; //We're the last file!
		stream->prev = NULL; //No next or previous!
	}
	changedealloc(stream, sizeof(*stream), &BIGFILEdealloc);
	unlock(LOCK_FILES);
	return (BIGFILE *)stream; //Opened!
}

int64_t emufwrite64(void *data,int64_t size,int64_t count,BIGFILE *stream)
{
	if (!stream) return -1; //Error!
	BIGFILE *b = (BIGFILE *)stream; //Convert!
	if (b->isappending) //Appending?
	{
		if (emuftell64(stream) != stream->size) //Not EOF?
		{
			if (emufseek64(stream, 0, SEEK_END) < 0) //Goto EOF before writing!
			{
				return 0; //Error out, we couldn't get to EOF!
			}
		}
	}
#ifdef IS_PS3
	u64 numwritten;
	if (size && count)
	{
		if (sysLv2FsWrite(b->f,data,size*count,&numwritten)<0) //Error?
		{
			return 0; //Error out!
		}
		//numwritten indicates how much has been written!
	}
	else
	{
		numwritten = 0; //Nothing to do!
	}
#else
#ifdef SDL3
	int64_t numwritten;
	if (size && count) //Valid?
	{
		numwritten = SDL_WriteIO(b->f, data, size*count); //Try to write, keep us compatible!
	}
	else
	{
		numwritten = 0;
	}
#else
	//SDL2?
	int64_t numwritten = SDL_RWwrite(b->f, data, 1, size*count); //Try to write, keep us compatible!
#endif
#endif

	if (numwritten>0) //No error?
	{
		b->position += numwritten; //Add to the position!
		numwritten /= size; //Divide by size to get equivalent!
	}
	if (b->position>b->size) //Overflow?
	{
		b->size = b->position; //Update the size!
	}
	return numwritten; //The size written!
}

int64_t emufread64(void* data, int64_t size, int64_t count, BIGFILE* stream)
{
	if (!stream) return -1; //Error!
	BIGFILE* b = (BIGFILE*)stream; //Convert!
#ifdef IS_PS3
	u64 pos;
#else
	Sint64 pos;
#endif
#ifdef IS_PS3
	u64 numread;
	if (size && count)
	{
		if (sysLv2FsRead(b->f,data,size*count,&numread)<0) //Error?
		{
			return 0; //Error out!
		}
		//numread indicates how much has been read!
	}
	else
	{
		numread = 0; //Nothing to do!
	}
#else
#ifdef SDL3
	int64_t numread;
	if (size && count) //Valid?
	{
		numread = SDL_ReadIO(b->f, data, size * count); //Try to write, keep us compatible!
	}
	else
	{
		numread = 0; //Nothing to do!
	}
#else
	int64_t numread = SDL_RWread(b->f, data, 1, size*count); //Try to write, keep us compatible!
#endif
#endif

	if (stream->isappending) //Are we in appending mode?
	{
		#ifdef IS_PS3
		pos = stream->position+numread; //We've moved what we've read!
		numread /= size; //Divide by size!
		#else
		#ifdef SDL3
		pos = SDL_TellIO(b->f); //Where are we?
		#else
		pos = SDL_RWtell(b->f); //Where are we?
		#endif
		#endif
		numread /= size; //Divide by size!
		if (pos>=0) //Where are we is a valid location?
		{
			stream->position = pos; //Update our position!
		}
		stream->isappending = 2; //Mark us as moved after appending!
	}
	else //Normal mode?
	{
		//We can safely assume our position!
		if (numread > 0) //No error?
		{
			b->position += numread; //Add to the position!
			numread /= size; //Divide by size!
		}
	}
	return numread; //The size read!
}

char fprintf64msg[4096];
char fprintf64msg2[4096 * 3]; //Up to thrice as large!

int_64 fprintf64(BIGFILE *fp, const char *format, ...)
{
	cleardata(&fprintf64msg[0],sizeof(fprintf64msg)); //Init!
	cleardata(&fprintf64msg2[0],sizeof(fprintf64msg2)); //Init!

	va_list args; //Going to contain the list!
	va_start (args, format); //Start list!
	safevsnprintf (fprintf64msg,sizeof(fprintf64msg), format, args); //Compile list!
	va_end (args); //Destroy list!

	#ifdef WINDOWS_LINEENDING
	uint_32 length;
	char *c;
	c = &fprintf64msg[0]; //Convert line endings appropriately!
	length = safestrlen(fprintf64msg, sizeof(fprintf64msg)); //Length!
	for (; length;) //Anything left?
	{
		if (*c) //Not end?
		{
			if ((*c == '\n') || (*c=='\r')) //Single-byte newline?
			{
				safescatnprintf(fprintf64msg2, sizeof(fprintf64msg2), "\r\n");
			}
			else if (*c!='\r') //Normal character? Treat as a normal character!
			{
				safescatnprintf(fprintf64msg2, sizeof(fprintf64msg2), "%c", *c);
			}
		}
		++c; //Next character!
		--length; //Next character!
	}
	#else
	memcpy(&fprintf64msg2, &fprintf64msg, sizeof(fprintf64msg)); //Same!
	#endif

	if (emufwrite64(&fprintf64msg2, 1, safestrlen(fprintf64msg2, sizeof(fprintf64msg2)), fp) != safestrlen(fprintf64msg2, sizeof(fprintf64msg2)))
	{
		return -1;
	}
	return safestrlen(fprintf64msg2, sizeof(fprintf64msg2)); //Result!
}

int getc64(BIGFILE *fp)
{
	sbyte result;
	if (emufeof64(fp)) //EOF?
	{
		return EOF; //EOF reached!
	}
	if (emufread64(&result, 1, 1, fp) != 1)
	{
		return -2; //Error out!
	}
	return (int)result;
}

int read_line64(BIGFILE* fp, char* bp, uint_32 bplength)
{
	FILEPOS filepos;
	int c = (int)'\0';
	int c2 = (int)'\0';
	uint_32 i = 0;
	/* Read one line from the source file */
	*bp = '\0'; //End of line: nothing loaded yet!
	if (emufeof64(fp)) return (0); //EOF reached?
	while ((c = getc64(fp)) != 0)
	{
		if ((c == '\n') || (c == '\r')) //Carriage return?
		{
			if (c == '\n') //Unix-style line-break?
			{
				break; //Stop searching: Unix-style newline detected!
			}
			//We're either a Windows-style newline or a Macintosh-style newline!
			filepos = emuftell64(fp); //File position to return to in case we're a Macintosh-style line break!
			c2 = getc64(fp); //Try the next byte to verify either a next line or Windows-style newline!
			if ((c == '\r') && (c2 == '\n')) //Windows newline detected?
			{
				break; //Stop searching: newline detected!
			}
			else //Macintosh newline followed by any character or EOF? Ignore which one, track back and apply!
			{
				emufseek64(fp, filepos, SEEK_SET); //Track back one character or EOF to the start of the new line!
				break; //Stop seaching: newline detected!
			}
		}
		if (i > (bplength - 1)) return (-1); //Overflow detected!
		if (c == EOF)         /* return FALSE on unexpected EOF */
		{
			bp[i] = '\0'; //End of line!
			return (1); //Finish up!
		}
		if (c == -1) //Error?
		{
			bp[i] = '\0'; //End of line!
			return (0); //Finish up!
		}
		if (likely(c)) //Valid character to add?
		{
			bp[i++] = (char)c;
		}
	}
	bp[i] = '\0';
	return (1);
}

int emufclose64(BIGFILE *stream)
{
	int result;
	if (!stream)
	{
		return -1; //Error!
	}
	BIGFILE *b = (BIGFILE *)stream; //Convert!
	if (!memprotect(b, sizeof(BIGFILE), "BIGFILE"))
	{
		return EOF; //Error: no file!
	}

	BIGFILE* next, *prev;
	lock(LOCK_FILES);
	next = stream->next;
	prev = stream->prev;
	if (next && prev) //Both?
	{
		next->prev = prev;
		prev->next = next;
		if (stream == lastopenedfile) //Last?
		{
			lastopenedfile = prev; //Last!
		}
	}
	else if (next) //Next only?
	{
		next->prev = NULL;
		if (stream == lastopenedfile) //Last?
		{
			lastopenedfile = next; //Last!
		}
	}
	else if (prev) //Previous only?
	{
		prev->next = NULL;
		if (stream == lastopenedfile) //Last?
		{
			lastopenedfile = prev; //Last!
		}
	}
	else //No next or prev?
	{
		stream->next = stream->prev = NULL; //No next or previous!
		lastopenedfile = NULL; //No last file!
	}

	char filename[256];
	memset(&filename[0],0,sizeof(filename));
	safestrcpy(filename,sizeof(filename),b->filename); //Set filename!
	result = 0; //Default result for success!
	if (b->ready)
	{
	#ifdef IS_PS3
		if (sysLv2FsClose(b->f)<0)
	#else
	#ifdef SDL3
		if (SDL_CloseIO(b->f) != 0)
	#else
		if (SDL_RWclose(b->f)!=0)
	#endif
	#endif
		{
			result = EOF;//Error closing the file!
		}
		b->ready = 0; //Became not ready anymore!
	}
	else
	{
		result = EOF; //Error closing the file!
	}

	changedealloc(stream, sizeof(*stream), getdefaultdealloc()); //Default deallocation!
	unlock(LOCK_FILES);
	freez((void **) &b, sizeof(*b), "fclose@Free_BIGFILE");//Free the object safely!
	if (b) //Still set?
	{
		result = EOF; //Error!
	}
	return result;//OK or error!
}